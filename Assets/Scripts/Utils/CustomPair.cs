﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Utils
{
    /**
     * Custom pair class is a simple container for pairs of values, especially used for the front- and backstack
     */
    public class CustomPair<T, U>
    {
        private T first;
        private U second;

        public CustomPair(T f, U s)
        {
            first = f;
            second = s;
        }

        public T GetFirst()
        {
            return first;
        }

        public U GetSecond()
        {
            return second;
        }

        /**
         * For stack renamed for better readability
         */
        public U GetForwardAction()
        {
            return second;
        }

        /**
        * For stack renamed for better readability
        */
        public T GetBackwardAction()
        {
            return first;
        }
    }
}
