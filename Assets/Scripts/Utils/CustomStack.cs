﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Utils
{
    /**
     * Custom stack class, has a readonly member of max size, if reached the peek is removed. The method PushMaintainingSize is the key method
     */
    public class CustomStack<T> : Stack<T>
    {
        private static readonly int MAX_SIZE = 3;
        public CustomStack()
            : base()
        {
        }

        /**
         * This method checks the maximum size of the stack, if it is reached, the peek gets removed and the stack is returned (overwriting this
         * is not possible). Otherwise it gets normally pushed and returned.
         */
        public CustomStack<T> PushMaintainingSize(T t)
        {
            if (Count >= MAX_SIZE)
            {
                CustomStack<T> temp = new CustomStack<T>();

                for (int i = 0; i < Count - 1; i++)
                {
                    temp.Push(Pop());
                }
                temp.Reverse();
                temp.Push(t);
                return temp;
            }
            else
            {
                Push(t);
                return this;
            }
        }

        /**
         * Reverses the order of the Stack
         */
        public CustomStack<T> Reverse()
        {
            CustomStack<T> reversed = new CustomStack<T>();
            for (int i = 0; i < Count; i++)
            {
                reversed.Push(Pop());
            }
            return reversed;
        }
    }
}
