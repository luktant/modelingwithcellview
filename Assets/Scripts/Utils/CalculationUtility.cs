﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public static class CalculationUtility
    {

        /************** Distribution Functions *******************/

        /**
         * Finds a surface point on the given compartment object. Works only for sphere surfaces. The resulting points are normally distributed
         */
        public static Vector4 DistributeOnSphereHull(CompartmentObject cO, Vector4 startPosition)
        {
            double theta = 2 * Math.PI * UnityEngine.Random.Range(0.0f, 1.0f);
            double psi = Math.Acos(2 * UnityEngine.Random.Range(0.0f, 1.0f) - 1);

            Vector4 PosOnSphere = new Vector4((float)(Math.Cos(theta) * Math.Sin(psi)) * (1 / cO.transform.localScale.x), (float)(Math.Sin(theta) * Math.Sin(psi)) * (1 / cO.transform.localScale.y), (float)Math.Cos(psi), 0) * (1 / cO.transform.localScale.z) / GlobalProperties.Get.Scale;
            PosOnSphere.w = startPosition.w;

            return PosOnSphere;
        }

        /**
         * Finds a surface point on any given compartment object, as long as the implemented signed distance function is correct. Takes random samples within its
         * bounding box, terminates at an average of about 250 iterations. 
         */
        public static Vector4 DistributeOnSurface(CompartmentObject cO, Vector4 startPosition)
        {
            Vector3 min = cO.GetBoundsMin();
            Vector3 max = cO.GetBoundsMax();

            Vector3 newPos = new Vector3(UnityEngine.Random.Range(min.x, max.x), UnityEngine.Random.Range(min.y, max.y), UnityEngine.Random.Range(min.z, max.z));

            while (!AlmostEquals(cO.GetSignedDistance(newPos), 0, 0.5f))
            {
                newPos = new Vector3(UnityEngine.Random.Range(min.x, max.x), UnityEngine.Random.Range(min.y, max.y), UnityEngine.Random.Range(min.z, max.z));
            }

            newPos /= GlobalProperties.Get.Scale;
            return new Vector4(newPos.x, newPos.y, newPos.z, startPosition.w);
        }

        /**
         * Distributes relative to the given compartment object. The resulting position is depending on the distance to the membrane and the boolean inside. If
         * the distance to the membrane is 0 the distribution occurs on the compartments surface, otherwise it has a certain distance to the membrane, either inside
         * or outside.
         */
        public static Vector4 DistributeRelativeToCompartment(CompartmentObject cO, Vector4 startPosition, float distanceToMembran, bool inside)
        {
            Vector3 distToMembran = (inside ? new Vector3(distanceToMembran, distanceToMembran, distanceToMembran) : Vector3.zero - new Vector3(distanceToMembran, distanceToMembran, distanceToMembran));

            Vector3 min = cO.GetBoundsMin();
            Vector3 max = cO.GetBoundsMax();

            //For root compartment increase max bounds to have some other stuff outside the cell
            if (cO.GetParent() == null && !inside)
            {
                max += max;
                min += min;
            }

            bool otherCompartmentsSatisfied = false;

            Vector3 newPos = new Vector3(UnityEngine.Random.Range(min.x, max.x), UnityEngine.Random.Range(min.y, max.y), UnityEngine.Random.Range(min.z, max.z));
            if (inside)
            {
                while (cO.GetSignedDistance(newPos) > 0 || !otherCompartmentsSatisfied)
                {
                    newPos = new Vector3(UnityEngine.Random.Range(min.x, max.x), UnityEngine.Random.Range(min.y, max.y), UnityEngine.Random.Range(min.z, max.z));
                    otherCompartmentsSatisfied = true;
                    foreach (CompartmentObject child in cO.GetChildren())
                    {
                        otherCompartmentsSatisfied = child.GetSignedDistance(newPos) > 0;
                        if (!otherCompartmentsSatisfied) break;
                    }
                }
            }
            else
            {
                while (cO.GetSignedDistance(newPos) < 0)
                {
                    newPos = new Vector3(UnityEngine.Random.Range(min.x, max.x), UnityEngine.Random.Range(min.y, max.y), UnityEngine.Random.Range(min.z, max.z));
                }
            }

            newPos /= GlobalProperties.Get.Scale;
            return new Vector4(newPos.x, newPos.y, newPos.z, startPosition.w);
        }
        /************** End Distribution Functions *******************/


        /************** Vector Calculations *******************/

        /*
         * Returns the surface point lying on the straight between the compartment center and the given point.  
         */
        public static Vector3 GetSurfacePoint(CompartmentObject referenceComp, Vector3 point)
        {
            if (point == Vector3.zero)
            {
                point = new Vector3(UnityEngine.Random.Range(referenceComp.GetBoundsMin().x, referenceComp.GetBoundsMax().x), UnityEngine.Random.Range(referenceComp.GetBoundsMin().y, referenceComp.GetBoundsMax().y), UnityEngine.Random.Range(referenceComp.GetBoundsMin().z, referenceComp.GetBoundsMax().z));
            }

            float distance = referenceComp.GetSignedDistance(point);
            Vector3 compartmentCenter = referenceComp.GetCenter();

            Vector3 direction = compartmentCenter - point;
            Vector3 normalizedDirection = Vector3.Normalize(direction);

            Vector3 toSurface = normalizedDirection * distance;
            Vector3 surfacePoint = point + toSurface;
            float dist = referenceComp.GetSignedDistance(surfacePoint);

            if ((referenceComp.GetSignedDistance(surfacePoint) < -0.5f || referenceComp.GetSignedDistance(surfacePoint) > 0.5f))
            {
                surfacePoint = GetSurfacePoint(referenceComp, surfacePoint);
            }

            return surfacePoint;
        }

        /*
         * Calculates recursive the closest surface point to the protein center, that means the surface point with the minimum distance 
         * to the protein center. The calculation starts with the surface point lying on the straight between the protein center and 
         * the compartment center. The method considers always a point next to the current point if the distance between the neighbor and 
         * the protein center is smaller then the current distance the method is called recursive with the neighboring point. The method tests 
         * eight neighboring points. If all distances are greater than the current the current point is returned as nearest surface point.
         * 
         */
        public static Vector3 GetNearestSurfacePoint(CompartmentObject referenceComp, Vector3 proteinCenter, Vector3 actN)
        {
            Vector3 actualNearest = actN;
            if (actualNearest == Vector3.zero)
            {
                actualNearest = GetSurfacePoint(referenceComp, proteinCenter);
            }
            Vector3 fromSurfaceToProtein = proteinCenter - actualNearest;

            float z = (fromSurfaceToProtein.x * 4 + fromSurfaceToProtein.y) / fromSurfaceToProtein.z;

            Vector3 perpendicular1 = Vector3.Normalize(new Vector3(4.0f, 4.0f, z));
            Vector3 perpendicular2 = Vector3.Normalize(Vector3.Cross(fromSurfaceToProtein, perpendicular1));
            Vector3 perpendicular3 = Vector3.Normalize(perpendicular1 + perpendicular2);
            Vector3 perpendicular4 = Vector3.Normalize(perpendicular1 + perpendicular2 * (-1));

            if ((proteinCenter - GetSurfacePoint(referenceComp, fromSurfaceToProtein + perpendicular1)).magnitude < fromSurfaceToProtein.magnitude)
            {
                return GetNearestSurfacePoint(referenceComp, proteinCenter, GetSurfacePoint(referenceComp, fromSurfaceToProtein + perpendicular1));
            }
            else if ((proteinCenter - GetSurfacePoint(referenceComp, fromSurfaceToProtein + perpendicular2)).magnitude < fromSurfaceToProtein.magnitude)
            {
                return GetNearestSurfacePoint(referenceComp, proteinCenter, GetSurfacePoint(referenceComp, fromSurfaceToProtein + perpendicular2));
            }
            else if ((proteinCenter - GetSurfacePoint(referenceComp, fromSurfaceToProtein + perpendicular3)).magnitude < fromSurfaceToProtein.magnitude)
            {
                return GetNearestSurfacePoint(referenceComp, proteinCenter, GetSurfacePoint(referenceComp, fromSurfaceToProtein + perpendicular3));
            }
            else if ((proteinCenter - GetSurfacePoint(referenceComp, fromSurfaceToProtein + perpendicular4)).magnitude < fromSurfaceToProtein.magnitude)
            {
                return GetNearestSurfacePoint(referenceComp, proteinCenter, GetSurfacePoint(referenceComp, fromSurfaceToProtein + perpendicular4));
            }
            else if ((proteinCenter - GetSurfacePoint(referenceComp, fromSurfaceToProtein + perpendicular1 * (-1))).magnitude < fromSurfaceToProtein.magnitude)
            {
                return GetNearestSurfacePoint(referenceComp, proteinCenter, GetSurfacePoint(referenceComp, fromSurfaceToProtein + perpendicular1 * (-1)));
            }
            else if ((proteinCenter - GetSurfacePoint(referenceComp, fromSurfaceToProtein + perpendicular2 * (-1))).magnitude < fromSurfaceToProtein.magnitude)
            {
                return GetNearestSurfacePoint(referenceComp, proteinCenter, GetSurfacePoint(referenceComp, fromSurfaceToProtein + perpendicular2 * (-1)));
            }
            else if ((proteinCenter - GetSurfacePoint(referenceComp, fromSurfaceToProtein + perpendicular3 * (-1))).magnitude < fromSurfaceToProtein.magnitude)
            {
                return GetNearestSurfacePoint(referenceComp, proteinCenter, GetSurfacePoint(referenceComp, fromSurfaceToProtein + perpendicular3 * (-1)));
            }
            else if ((proteinCenter - GetSurfacePoint(referenceComp, fromSurfaceToProtein + perpendicular4 * (-1))).magnitude < fromSurfaceToProtein.magnitude)
            {
                return GetNearestSurfacePoint(referenceComp, proteinCenter, GetSurfacePoint(referenceComp, fromSurfaceToProtein + perpendicular4 * (-1)));
            }
            else
            {
                return actualNearest;
            }
        }

        /*
         * Calculates the normal vector in the given point on the surfeace of the given compartment.
         * The normal vector is calculated with the difference quotient using central difference.  
         */
        public static Vector3 GetNormalVectorNew(CompartmentObject referenceComp, Vector3 surfacePoint)
        {
            Vector3 a = surfacePoint + new Vector3(1, 0, 0);
            Vector3 b = surfacePoint - new Vector3(1, 0, 0);
            Vector3 c = surfacePoint + new Vector3(0, 1, 0);
            Vector3 d = surfacePoint - new Vector3(0, 1, 0);
            Vector3 e = surfacePoint + new Vector3(0, 0, 1);
            Vector3 f = surfacePoint - new Vector3(0, 0, 1);

            Vector3 nV = new Vector3((referenceComp.GetSignedDistance(a) - referenceComp.GetSignedDistance(b))/2,
                                     (referenceComp.GetSignedDistance(c) - referenceComp.GetSignedDistance(d))/2,
                                     (referenceComp.GetSignedDistance(e) - referenceComp.GetSignedDistance(f))/2);

            return nV;
        }

        /************** End Vector Calculations *******************/


        /************** Almost Equals Calculations *******************/
        /**
         * Almost equals method, taking two Vector4 parameters, returning whether they equal with the given precision.
         */
        public static bool AlmostEquals(Vector4 v1, Vector4 v2, float precision)
        {
            return (Math.Abs(v1.x - v2.x) <= precision && Math.Abs(v1.y - v2.y) <= precision && Math.Abs(v1.z - v2.z) <= precision);
        }

        /**
         * Almost equals method, taking two float parameters, returning whether they equal with the given precision.
         */
        public static bool AlmostEquals(float f1, float f2, float precision)
        {
            return (Math.Abs(f1 - f2) <= precision);
        }

        /**
         * Almost equals method, returns if the given quaternions are almost equal relative to the compartment surface. 
         */
        public static bool AlmostEquals(Quaternion q1, Vector3 p1, Quaternion q2, Vector3 p2, CompartmentObject comp)
        {
            Vector3 nearestSurfacePoint1 = GetNearestSurfacePoint(comp, p1, Vector3.zero);
            Vector3 nearestSurfacePoint2 = GetNearestSurfacePoint(comp, p1, Vector3.zero);

            Vector3 normalVector1 = GetNormalVectorNew(comp, nearestSurfacePoint1);
            Vector3 normalVector2 = GetNormalVectorNew(comp, nearestSurfacePoint2);

            Vector3 yAxis1 = q1 * new Vector3(0, 1, 0);
            Vector3 yAxis2 = q2 * new Vector3(0, 1, 0);

            if (AlmostEquals(Vector3.Angle(normalVector1, yAxis1), Vector3.Angle(normalVector2, yAxis2), 1.0f))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /************** End Almost Equals Calculations *******************/


        /************** Quaternion Utility *******************/
        // Quaternion Calculation if realy needed may outsource to MyUtility

        /*
         * Retruns the rotations axis
         */
        public static Vector3 GetRotationAxis(Quaternion q)
        {
            float rotationAngle = Mathf.Acos(q.w) * 2;
            float x = q.x / Mathf.Sin(rotationAngle / 2);
            float y = q.y / Mathf.Sin(rotationAngle / 2);
            float z = q.z / Mathf.Sin(rotationAngle / 2);

            return new Vector3(x, y, z);
        }

        /*
         * Returns the rotation angle of the quaternion q 
         */
        public static float GetRotationAngle(Quaternion q)
        {
            float rotationAngle = Mathf.Acos(q.w) * 2;
            return rotationAngle;
        }

        /*
         * Returns the quaternion which describes the rotation between two vectors 
         */
        public static Quaternion GetRotationBetweenTwoVec(Vector3 from, Vector3 to)
        {
            from = Vector3.Normalize(from);
            to = Vector3.Normalize(to);

            float cosTheta = Vector3.Dot(from, to);
            Vector3 rotationAxis;

            //|| cosTheta < 0 + 0.04f && cosTheta > 0 - 0.04f||cosTheta>1-0.04f
            if (cosTheta < -1 + 0.001f)
            {
                rotationAxis = Vector3.Cross(new Vector3(0, 0, 1), from);
                if (rotationAxis.sqrMagnitude < 0.01)
                {

                    rotationAxis = Vector3.Cross(new Vector3(1, 0, 0), from);
                }
                rotationAxis = Vector3.Normalize(rotationAxis);
                return Quaternion.AngleAxis(180f, rotationAxis);
            }
            rotationAxis = Vector3.Cross(from, to);

            float s = Mathf.Sqrt((1 + cosTheta) * 2);
            float inverse = 1 / s;

            return new Quaternion(rotationAxis.x * inverse, rotationAxis.y * inverse, rotationAxis.z * inverse, s * 0.5f);
        }

        /*
         * Returns the new rotation quaternion for the given protein position stored in a 4D vector. The rotation is calculated
         * relative to the corresponding normal vector. The RotationProbabilityPair contains the information of the reference compartment, 
         * normal vector and rotation. 
         */
        public static Vector4 GetNewRotation(Vector4 proteinPosition, RotationProbabilityPair rPP)
        {
            float variance = DecisionTreeManager.Get.GetIngredientVariance((int)proteinPosition.w);
            Vector4 rotvec;
            Vector3 protPos = new Vector3(proteinPosition.x, proteinPosition.y, proteinPosition.z) * GlobalProperties.Get.Scale;

            CompartmentObject tempComp = rPP.GetReferenceCompartent();

            Vector3 surfacePoint = CalculationUtility.GetNearestSurfacePoint(tempComp, protPos, Vector3.zero);
            Vector3 normalVector = Vector3.Normalize(CalculationUtility.GetNormalVectorNew(tempComp, surfacePoint));

            Quaternion rotationBetweenNormals = Quaternion.FromToRotation(rPP.GetReferenceNormalVector(), normalVector);

            if (rPP.relativeToCompartment)
            {

                // rotation to y axis 
                Vector3 hy = Vector3.Normalize(rotationBetweenNormals * rPP.GetReferenceYAxis());
                Vector3 proteineYAxis = Vector3.Normalize(new Vector3(0, 1, 0));

                Quaternion hyRotateToY = CalculationUtility.GetRotationBetweenTwoVec(proteineYAxis, hy);
                //UnityEngine.Debug.Log("RotationNYY:" + hyRotateToY);
                // rotation to x axis 
                Vector3 hx = Vector3.Normalize(rotationBetweenNormals * rPP.GetReferenceXAxis());
                Vector3 proteineXAxis = Vector3.Normalize(hyRotateToY * new Vector3(1, 0, 0));

                Quaternion hxRotateToX = CalculationUtility.GetRotationBetweenTwoVec(proteineXAxis, hx);

                Vector3 testaxis = CalculationUtility.GetRotationAxis(hxRotateToX);
                Quaternion endRotation = hyRotateToY * hxRotateToX;
                //UnityEngine.Debug.Log("RAN:" + Vector3.Angle(normalVector, GetRotationAxis(endRotation)));
                //UnityEngine.Debug.Log("NY:"+Vector3.Angle(normalVector, hy));
                //UnityEngine.Debug.Log("NX:" + Vector3.Angle(normalVector, hx));
                rotvec = MyUtility.QuanternionToVector4(hxRotateToX * hyRotateToY);


            }
            else
            {
                bool relativToMembrane = true;
                if (relativToMembrane)
                {
                    Quaternion rotationBetweenNormalsNegative = Quaternion.FromToRotation(rPP.GetReferenceNormalVector(), normalVector);

                    // rotation to y axis 
                    Vector3 hy = Vector3.Normalize(rotationBetweenNormalsNegative * rPP.GetReferenceYAxis());
                    Vector3 proteineYAxis = Vector3.Normalize(new Vector3(0, 1, 0));

                    Quaternion hyRotateToY = CalculationUtility.GetRotationBetweenTwoVec(proteineYAxis, hy);

                    // rotation to x axis 
                    Vector3 hx = Vector3.Normalize(rotationBetweenNormalsNegative * rPP.GetReferenceXAxis());
                    Vector3 proteineXAxis = Vector3.Normalize(hyRotateToY * new Vector3(1, 0, 0));

                    Quaternion hxRotateToX = CalculationUtility.GetRotationBetweenTwoVec(proteineXAxis, hx);

                    Vector3 testaxis = CalculationUtility.GetRotationAxis(hxRotateToX);
                    Quaternion endRotation = hxRotateToX * hyRotateToY;
                    //GetRotationAngle(endRotation);
                    rotvec = MyUtility.QuanternionToVector4(hxRotateToX * hyRotateToY);
                }
                else
                {

                    //Vector3 parentCenter = rPP.GetReferenceCompartent().GetCenter();
                    Vector3 parentCenter = Vector3.zero;
                    Vector3 transformVector = Vector3.zero - rPP.GetReferenceCompartent().GetCenter();
                    //Vector3 transformVector = Vector3.zero;
                    Vector3 proteinCenter = new Vector3(proteinPosition.x, proteinPosition.y, proteinPosition.z) * GlobalProperties.Get.Scale;

                    float f = rPP.GetReferenceCompartent().GetSignedDistance(Vector3.zero);

                    Vector3 directionVector = Vector3.Normalize((proteinCenter + transformVector) - parentCenter);
                    Quaternion rotationBetweenDirectionV = CalculationUtility.GetRotationBetweenTwoVec(rPP.GetReferenceNormalVector(), directionVector);

                    // rotation to y axis 
                    Vector3 hy = Vector3.Normalize(rotationBetweenDirectionV * rPP.GetReferenceYAxis());
                    Vector3 proteineYAxis = Vector3.Normalize(new Vector3(0, 1, 0));

                    Quaternion hyRotateToY = CalculationUtility.GetRotationBetweenTwoVec(proteineYAxis, hy);
                    //Quaternion hyRotateToY = Quaternion.FromToRotation(proteineYAxis, hy);
                    // rotation to x axis 
                    Vector3 hx = Vector3.Normalize(rotationBetweenDirectionV * rPP.GetReferenceXAxis());
                    Vector3 proteineXAxis = Vector3.Normalize(hyRotateToY * new Vector3(1, 0, 0));

                    Quaternion hxRotateToX = CalculationUtility.GetRotationBetweenTwoVec(proteineXAxis, hx);
                    //Quaternion hxRotateToX = Quaternion.FromToRotation(proteineXAxis, hx);
                    rotvec = MyUtility.QuanternionToVector4(hxRotateToX * hyRotateToY);
                    //UnityEngine.Debug.Log("DV:" + directionVector.x + "," + directionVector.y + "," + directionVector.z);
                    //UnityEngine.Debug.Log("YA:" + proteineXAxis.x + "," + proteineXAxis.y + "," + proteineXAxis.z);
                    //UnityEngine.Debug.Log(Vector3.Angle(directionVector, proteineYAxis));
                    //UnityEngine.Debug.Log(Vector3.Dot(directionVector, proteineYAxis));
                }
            }

            return rotvec;
        }

        /*
         * Returns a rotation quaternion which rotation is relative to the straight between the center of the compartment and 
         * protein position. The RotationProbabilityPair cotains  the information of the reference compartment and the reference rotation.
         */ 
        public static Vector4 GetNewRotationRelativeToCenter(Vector4 proteinPosition, RotationProbabilityPair rPP)
        {
            Vector3 parentCenter = Vector3.zero;
            Vector3 transformVector = Vector3.zero - rPP.GetReferenceCompartent().GetCenter();
            Vector3 proteinCenter = new Vector3(proteinPosition.x, proteinPosition.y, proteinPosition.z);

            float f = rPP.GetReferenceCompartent().GetSignedDistance(Vector3.zero);

            Vector3 directionVector = Vector3.Normalize((proteinCenter + transformVector) - parentCenter);
            Quaternion rotationBetweenDirectionV = CalculationUtility.GetRotationBetweenTwoVec(rPP.GetReferenceNormalVector(), directionVector);

            // rotation to y axis 
            Vector3 hy = Vector3.Normalize(rotationBetweenDirectionV * rPP.GetReferenceYAxis());
            Vector3 proteineYAxis = Vector3.Normalize(new Vector3(0, 1, 0));

            Quaternion hyRotateToY = CalculationUtility.GetRotationBetweenTwoVec(proteineYAxis, hy);
            // rotation to x axis 
            Vector3 hx = Vector3.Normalize(rotationBetweenDirectionV * rPP.GetReferenceXAxis());
            Vector3 proteineXAxis = Vector3.Normalize(hyRotateToY * new Vector3(1, 0, 0));

            Quaternion hxRotateToX = CalculationUtility.GetRotationBetweenTwoVec(proteineXAxis, hx);
            return MyUtility.QuanternionToVector4(hxRotateToX * hyRotateToY);
        }
        /************** End Quaternion Utility *******************/

    }

}
