﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Utils
{
    /**
     * The custom triple is a simple container for a triple of values.
     */
    public class CustomTriple<T, U, V>
    {
        private T first;
        private U second;
        private V third;

        public CustomTriple(T f, U s, V t)
        {
            first = f;
            second = s;
            third = t;
        }

        public T GetFirst()
        {
            return first;
        }

        public U GetSecond()
        {
            return second;
        }

        public V GetThird()
        {
            return third;
        }

        /**
         * Returns a custom pair of the values of this triple
         */
        public CustomPair<U, V> CreateCustomPairFromValues()
        {
            return new CustomPair<U, V>(second, third);
        }
    }
}
