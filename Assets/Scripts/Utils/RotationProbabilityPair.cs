﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Newtonsoft.Json;

namespace Assets.Scripts.Utils
{
    /*
     * interval of length 5  
     */
    [JsonObject(MemberSerialization.OptIn)]
    public class RotationProbabilityPair
    {
        /*
         * A RotationPropability pair is an object which capsules all information from a reference rotation which is used to calculate the rotations of the other proteins.  It is used in the rotation approach without reference space.  
         */
        [JsonProperty]
        public float rotated = 0;

        [JsonProperty]
        public string refCompartment;

        public CompartmentObject referenceCompartment;

        [JsonProperty]
        public Vector3 normalVector;
        [JsonProperty]
        public Vector3 referenceYAxis;
        [JsonProperty]
        public Vector3 referenceXAxis;
        [JsonProperty]
        public Quaternion rotation;

        [JsonProperty]
        public bool relativeToCompartment;

        // @refernce vector ... normal vector or direction vector 
        /*
         * Constructor. If the rotation is relative to a compartment the reference vector contains the normal Vector in the closest surface point of current rotted object otherwise
         * it contains the direction vector from the center of the parent compartment to the center of the current rotated object. The Objects have their own local coordinate system 
         * which is rotated with the objet, x- and y- axis are the x- and y- axis from the object coordinate system. Rotation contains the rotation quaternion from the user affected 
         * rotation.
         */
        public RotationProbabilityPair(Quaternion rotation, Vector3 referenceVctor, Vector3 xAxis, Vector3 yAxis, CompartmentObject referenceCompartment, bool relativeToCompartment)
        {
            this.rotation = rotation;
            this.normalVector = referenceVctor;
            this.referenceXAxis = xAxis;
            this.referenceYAxis = yAxis;
            this.referenceCompartment = referenceCompartment;
            this.relativeToCompartment = relativeToCompartment;
            this.refCompartment = referenceCompartment.name;
        }

        /**
         * Sets the compartment object. The rotation is calculated relative to the surface of that compartment. 
         */ 
        public void SetReferenceCompartment(CompartmentObject comp)
        {
            referenceCompartment = comp;
        }

        public CompartmentObject GetReferenceCompartent()
        {
            return referenceCompartment;
        }

        /*
         * Returns the amount of the proteins which are rotated by the user as described by that RotationProbabilityPair   
         */
        public float GetRotated()
        {
            return rotated;
        }

        /*
         * Increments the amount of user affected rotations described by that RotationProbabilityPair. Called if the user rotates another protein repeateldy almost the same. 
         */
        public void IncrementRotated()
        {
            rotated++;
        }

        /*
         * Decrements the amount of user affected rotations described by that RotationProbabilityPair. Only called for undo operations.
         */
        public void DecrementRotated()
        {
            rotated--;
        }

        /*
         * Returns the normal vector of the reference rotation which was used to build that RotationProbabilityPair.  
         */
        public Vector3 GetReferenceNormalVector()
        {
            return normalVector;
        }

        /*
         * Returns the y-axis of the reference rotation which was used to build that RotationProbabilityPair.  
         */
        public Vector3 GetReferenceYAxis()
        {
            return referenceYAxis;
        }

        /*
         * Returns the x-axis of the reference rotation which was used to build that RotationProbabilityPair.  
         */
        public Vector3 GetReferenceXAxis()
        {
            return referenceXAxis;
        }

        /*
        * Returns the rotation quaternion of the reference rotation which was used to build that RotationProbabilityPair.  
        */
        public Quaternion GetRotation()
        {
            return rotation;
        }

        /*
         * Almost equals Method for RotationProbabilityPairs
         */
        public bool Equals(RotationProbabilityPair otherPair)
        {
            if (CalculationUtility.AlmostEquals(Vector3.Angle(normalVector, referenceYAxis), Vector3.Angle(otherPair.normalVector, otherPair.referenceYAxis), 2.0f) && CalculationUtility.AlmostEquals(Vector3.Angle(normalVector, referenceXAxis), Vector3.Angle(otherPair.normalVector, otherPair.referenceXAxis), 2.0f))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}