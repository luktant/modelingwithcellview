﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public class KDEUtility : MonoBehaviour
    {
        // Declare as a singelton 
        private static KDEUtility _instance = null;

        public Dictionary<Quaternion, int> samples = new Dictionary<Quaternion, int>();


        public static KDEUtility Get
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new KDEUtility();
                }
                return _instance;
            }
        }

        

        public static Quaternion GetKDERandomQuaternion(Dictionary<Quaternion,int> samples, float varianz)
        {

            return Quaternion.identity;
        }

    }
}
