﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using System.IO;
using Newtonsoft.Json;
using Assets.Scripts.Misc;

namespace Assets.Scripts.Utils
{
    public class SceneFileUtility : EditorWindow
    {
        /**
         * Exports all neccessary information about the scene (ingredients with position, rotation and information, clusters, decision tree, compartments)
         * Can be reloaded and the modelling process can continue. The generated file has the ending .csv (cellviewfile)
         */
        public static void ExportScene()
        {
            string path = EditorUtility.SaveFilePanel("Save as", "", "", "cvf");

            if (path.Length != 0)
            {
                EditorUtility.DisplayProgressBar("This process may take some seconds.", "Creating file", 0.0f);

                List<JsonSerializableObj> content = new List<JsonSerializableObj>();

                float stepsize = 1.0f / (CPUBuffers.Get.ProteinInstancePositions.Count() + CompartmentObjectManager.Get.CompartmentObjects.Count());
                float currentProgress = 0.0f;

                List<int> compIds = SceneCreatorUIController.Get.lockedIngredientIds;
                //Store all proteins
                for (int i = 0; i < CPUBuffers.Get.ProteinInstancePositions.Count(); i++)
                {
                    EditorUtility.DisplayProgressBar("This process may take some seconds.", "Writing protein " + i + " ...", currentProgress);
                    JsonSerializableObj obj = new JsonSerializableObj();

                    Protein p = new Protein();
                    p.Position = CPUBuffers.Get.ProteinInstancePositions[i];
                    p.Rotation = CPUBuffers.Get.ProteinInstanceRotations[i];
                    p.Information = CPUBuffers.Get.ProteinInstanceInfos[i];

                    obj.ObjType = compIds.Contains((int)p.Position.w) ? ObjType.MEMBRANE : ObjType.PROTEIN;
                    obj.pObj = p;

                    content.Add(obj);

                    currentProgress += stepsize;
                }
                //Store compartment information
                for (int i = CompartmentObjectManager.Get.CompartmentObjects.Count() - 1; i >= 0; i--)
                {
                    EditorUtility.DisplayProgressBar("This process may take some seconds.", "Writing compartment " + CompartmentObjectManager.Get.CompartmentObjects[i].GetName() + " ...", currentProgress);

                    JsonSerializableObj obj = new JsonSerializableObj();
                    obj.ObjType = ObjType.COMPARTMENT;
                    obj.cObj = GenerateCompartmentRef(CompartmentObjectManager.Get.CompartmentObjects[i]);

                    content.Add(obj);
                    currentProgress += stepsize;
                }

                JsonSerializableObj decTreeObj = new JsonSerializableObj();
                decTreeObj.ObjType = ObjType.DECISION_TREE;
                decTreeObj.dTree = GenerateDecisionTreeRef();
                content.Add(decTreeObj);

                JsonSerializableObj clusterMan = new JsonSerializableObj();
                clusterMan.ObjType = ObjType.CLUSTER_SET;
                clusterMan.cMan = ClusterManager.clusters;
                content.Add(clusterMan);

                EditorUtility.DisplayProgressBar("This process may take some seconds.", "Finishing file...", 1.0f);

                File.WriteAllText(path, JsonConvert.SerializeObject(content, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    }));

                EditorUtility.ClearProgressBar();
            }
        }

        /**
         * Exports a template, a template is a cluster of proteins with a certain shape. This template can be used in the modelling process to create a
         * unique membrane without the hassle of learning a certain pattern.
         */
        public static void ExportTemplate()
        {
            //First move the positions to center
            Vector4 center = Vector4.zero;

            for (int i = 0; i < CPUBuffers.Get.ProteinInstancePositions.Count; i++)
            {
                center += CPUBuffers.Get.ProteinInstancePositions[i];
            }
            center /= CPUBuffers.Get.ProteinInstancePositions.Count;
            center.w = 0;

            string path = EditorUtility.SaveFilePanel("Save as", "", "", "template");

            if (path.Length != 0)
            {
                EditorUtility.DisplayProgressBar("This process may take some seconds.", "Creating file", 0.0f);

                List<JsonSerializableObj> content = new List<JsonSerializableObj>();

                float stepsize = 1.0f / CPUBuffers.Get.ProteinInstancePositions.Count();
                float currentProgress = 0.0f;

                //Store all proteins
                for (int i = 0; i < CPUBuffers.Get.ProteinInstancePositions.Count(); i++)
                {
                    EditorUtility.DisplayProgressBar("This process may take some seconds.", "Writing protein " + i + " ...", currentProgress);
                    JsonSerializableObj obj = new JsonSerializableObj();

                    Vector3[] rotationArray = GetRelativeRotation(CPUBuffers.Get.ProteinInstanceRotations[i], CPUBuffers.Get.ProteinInstancePositions[i]);

                    Vector4 rotation = CPUBuffers.Get.ProteinInstanceRotations[i];

                    Protein p = new Protein();
                    p.Position = CPUBuffers.Get.ProteinInstancePositions[i] - center;
                    p.Rotation = CPUBuffers.Get.ProteinInstanceRotations[i];
                    p.Information = CPUBuffers.Get.ProteinInstanceInfos[i];

                    p.ReferenceOrientationAxis = rotationArray[0];
                    p.ReferenceYAxis = rotationArray[1];
                    p.ReferenceXAxis = rotationArray[2];

                    obj.ObjType = ObjType.PROTEIN;
                    obj.pObj = p;

                    content.Add(obj);

                    currentProgress += stepsize;
                }

                EditorUtility.DisplayProgressBar("This process may take some seconds.", "Finishing file...", 1.0f);

                File.WriteAllText(path, JsonConvert.SerializeObject(content, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    }));

                EditorUtility.ClearProgressBar();
            }
        }
        /**
         * Creates a vector array to export the relevant information for the rotation. The first vector contains the 
         * center of the protein, the second the y-axis and the third the x-axis. 
         */
        public static Vector3[] GetRelativeRotation(Vector4 oldRot, Vector4 proteinCenter)
        {
            Quaternion oR = MyUtility.Vector4ToQuaternion(oldRot);
            Vector3 dV = proteinCenter;
            Vector3 yA = Vector3.Normalize(oR * new Vector3(0, 1, 0));
            Vector3 xA = Vector3.Normalize(oR * new Vector3(1, 0, 0));
            Vector3[] vectorArray = { dV, yA, xA };
            return vectorArray;
        }

        /**
         * Used to import a certain template, this method is called when the user decides to use a template as membrane for a compartment.
         */
        public static List<Protein> ImportTemplate()
        {
            List<Protein> template = new List<Protein>();

            string path = EditorUtility.OpenFilePanel("Open file", "", "template");

            if (path.Length != 0)
            {
                EditorUtility.DisplayProgressBar("This process may take some seconds.", "Loading file...", 0.0f);

                float currentProgress = 0.0f;
                int i = 0;

                String text = ConvertStringArrayToString(File.ReadAllLines(path));
                List<JsonSerializableObj> content = JsonConvert.DeserializeObject<List<JsonSerializableObj>>(text);

                float stepsize = 1.0f / content.Count();

                foreach (JsonSerializableObj obj in content)
                {
                    currentProgress += stepsize;
                    i++;

                    if (obj.ObjType == ObjType.PROTEIN)
                    {
                        EditorUtility.DisplayProgressBar("This process may take some seconds.", "File loaded, adding protein to scene...", currentProgress);
                        Protein p = new Protein();
                        p.Position = obj.pObj.Position;
                        p.Rotation = obj.pObj.Rotation;
                        p.Information = obj.pObj.Information;
                        p.ReferenceXAxis = obj.pObj.ReferenceXAxis;
                        p.ReferenceYAxis = obj.pObj.ReferenceYAxis;
                        p.ReferenceOrientationAxis = obj.pObj.ReferenceOrientationAxis;

                        template.Add(p);
                    }
                }

                EditorUtility.DisplayProgressBar("This process may take some seconds.", "Loading finished", 1.0f);
            }
            EditorUtility.ClearProgressBar();
            return template;
        }

        /**
         * Imports all neccessary information about the scene (ingredients with position, rotation and information, clusters, decision tree, 
         * compartments) from a given cvf file.
         */
        public static void ImportScene()
        {
            string path = EditorUtility.OpenFilePanel("Open file", "", "cvf");

            if (path.Length != 0)
            {
                EditorUtility.DisplayProgressBar("This process may take some seconds.", "Loading file...", 0.0f);

                float currentProgress = 0.0f;
                int i = 0;

                String text = ConvertStringArrayToString(File.ReadAllLines(path));
                List<JsonSerializableObj> content = JsonConvert.DeserializeObject<List<JsonSerializableObj>>(text);

                List<int> compIds = new List<int>();

                //Holds the information, after loading parent-child relationships have to be reconstructed
                List<CompartmentRef> compartments = new List<CompartmentRef>();

                List<CompartmentObject> restoredCompartments = new List<CompartmentObject>();

                float stepsize = 1.0f / content.Count();

                foreach (JsonSerializableObj obj in content)
                {
                    currentProgress += stepsize;
                    i++;

                    if (obj.ObjType == ObjType.PROTEIN || obj.ObjType == ObjType.MEMBRANE)
                    {
                        EditorUtility.DisplayProgressBar("This process may take some seconds.", "File loaded, adding protein to scene...", currentProgress);

                        CPUBuffers.Get.ProteinInstancePositions.Add(obj.pObj.Position);
                        CPUBuffers.Get.ProteinInstanceRotations.Add(obj.pObj.Rotation);
                        CPUBuffers.Get.ProteinInstanceInfos.Add(obj.pObj.Information);

                        if (obj.ObjType == ObjType.MEMBRANE && !compIds.Contains((int)obj.pObj.Position.w)) compIds.Add((int)obj.pObj.Position.w);
                    }
                    else if (obj.ObjType == ObjType.COMPARTMENT)
                    {
                        EditorUtility.DisplayProgressBar("This process may take some seconds.", "File loaded, adding compartment to scene...", currentProgress);
                        compartments.Add(obj.cObj);
                        restoredCompartments.Add(ExtractFromCompartmentRef(obj.cObj));
                    }
                    else if (obj.ObjType == ObjType.DECISION_TREE)
                    {
                        EditorUtility.DisplayProgressBar("This process may take some seconds.", "File loaded, restoring decision tree...", currentProgress);
                        RestoreDecisionTree(obj.dTree);
                    }
                    else if (obj.ObjType == ObjType.CLUSTER_SET)
                    {
                        ClusterManager.clusters = obj.cMan;
                    }
                }

                EditorUtility.DisplayProgressBar("This process may take some seconds.", "Loading finished, finishing scene...", 1.0f);

                RestoreCompartmentHierarchy(compartments, restoredCompartments);
                CompartmentObjectManager.Get.CompartmentObjects.AddRange(restoredCompartments);

                foreach (KeyValuePair<int, TreeNode> subTree in DecisionTreeManager.Get.rootNodes)
                {
                    foreach (TreeNode node in DecisionTreeManager.Get.rootNodes[subTree.Key].GetNodes())
                    {
                        foreach (CompartmentObject cO in CompartmentObjectManager.Get.CompartmentObjects)
                        {
                            if (cO.name.Equals(node.compObjName)) node.compObj = cO;
                            foreach (RotationProbabilityPair pair in node.rotationPropabilities)
                            {
                                if (cO.name.Equals(pair.refCompartment))
                                {
                                    pair.referenceCompartment = cO;
                                }
                            }
                        }
                    }
                }

                foreach (KeyValuePair<int, ProteinCluster> cluster in ClusterManager.clusters)
                {
                    foreach (CompartmentObject cO in CompartmentObjectManager.Get.CompartmentObjects)
                    {
                        if (cO.name.Equals(cluster.Value.compObjName)) cluster.Value.compartment = cO;
                        foreach (RotationProbabilityPair pair in cluster.Value.clusterRotationPropabilities)
                        {
                            if (cO.name.Equals(pair.refCompartment))
                            {
                                pair.referenceCompartment = cO;
                            }
                        }
                    }
                }

                CPUBuffers.Get.CopyDataToGPU();
            }
            EditorUtility.ClearProgressBar();
        }

        /**
         * Converts a string array to a content string, each entry gets a new line
         */
        private static string ConvertStringArrayToString(string[] array)
        {
            StringBuilder builder = new StringBuilder();
            foreach (string value in array)
            {
                builder.Append(value);
                builder.Append("\n");
            }
            return builder.ToString();
        }

        /**
         * Creates a serializable CompartmentRef object, containing the neccesary information from the given compartment object.
         */
        private static CompartmentRef GenerateCompartmentRef(CompartmentObject cO)
        {
            CompartmentRef cmpt = new CompartmentRef();

            if (cO.GetParent() != null) cmpt.Parent = cO.GetParent().GetName();

            List<String> children = new List<String>();
            foreach (CompartmentObject child in cO.GetChildren())
            {
                children.Add(child.GetName());
            }
            cmpt.Children = children;
            cmpt.CompartmentType = cO.compartmentType;

            switch (cO.compartmentType)
            {
                case CompartmentType.Ellipsoid:
                    cmpt.RadiiEllipsoid = ((EllipsoidCompartment)cO).radii;
                    break;
                case CompartmentType.Capsule:
                    cmpt.CapsuleA = ((CapsuleCompartment)cO).capsuleA;
                    cmpt.CapsuleB = ((CapsuleCompartment)cO).capsuleB;
                    cmpt.RadiiEllipsoid = ((CapsuleCompartment)cO).capsuleRadius;
                    break;
                case CompartmentType.Torus:
                    cmpt.RadiiRing = ((TorusCompartment)cO).radiiRing;
                    cmpt.RadiiEllipsoid = ((TorusCompartment)cO).radiiSphere;
                    break;
            }

            cmpt.MembraneId = cO.membraneId;
            cmpt.MembraneAmount = cO.GetMembraneAmount();
            cmpt.Center = cO.GetCenter();
            cmpt.BbMinX = cO.boundingBox.minX;
            cmpt.BbMinY = cO.boundingBox.minY;
            cmpt.BbMinZ = cO.boundingBox.minZ;
            cmpt.BbMaxX = cO.boundingBox.maxX;
            cmpt.BbMaxY = cO.boundingBox.maxY;
            cmpt.BbMaxZ = cO.boundingBox.maxZ;
            cmpt.Name = cO.name;

            return cmpt;
        }

        /**
         * Creates CompartmentObjects from the given information. The hierarchy is restored later, because it is not sure that all information 
         * is loaded when setting a parent or children
         */
        private static CompartmentObject ExtractFromCompartmentRef(CompartmentRef cR)
        {
            GameObject compartmentObject = new GameObject();
            CompartmentObject compartment;
            switch (cR.CompartmentType)
            {
                case CompartmentType.Capsule:
                    compartment = (CapsuleCompartment)compartmentObject.AddComponent<CapsuleCompartment>();
                    ((CapsuleCompartment)compartment).capsuleA = cR.CapsuleA;
                    ((CapsuleCompartment)compartment).capsuleB = cR.CapsuleB;
                    ((CapsuleCompartment)compartment).capsuleRadius = cR.RadiiEllipsoid;
                    break;
                case CompartmentType.Torus:
                    compartment = (TorusCompartment)compartmentObject.AddComponent<TorusCompartment>();
                    ((TorusCompartment)compartment).radiiRing = cR.RadiiRing;
                    ((TorusCompartment)compartment).radiiSphere = cR.RadiiEllipsoid;
                    break;
                default: //Assume it is ellipsoid
                    compartment = (EllipsoidCompartment)compartmentObject.AddComponent<EllipsoidCompartment>();
                    ((EllipsoidCompartment)compartment).radii = cR.RadiiEllipsoid;
                    break;
            }
            compartment.name = cR.Name;
            compartment.membraneId = cR.MembraneId;
            compartment.membraneAmount = cR.MembraneAmount;
            compartment.center = cR.Center;
            compartment.boundingBox.minX = cR.BbMinX;
            compartment.boundingBox.minY = cR.BbMinY;
            compartment.boundingBox.minZ = cR.BbMinZ;
            compartment.boundingBox.maxX = cR.BbMaxX;
            compartment.boundingBox.maxY = cR.BbMaxY;
            compartment.boundingBox.maxZ = cR.BbMaxZ;

            return compartment;
        }

        /*
         * Restores the compartment hierarchy, after all are loaded the hierarchy can be reconstructed.
         */
        private static void RestoreCompartmentHierarchy(List<CompartmentRef> compartments, List<CompartmentObject> restoredCompartments)
        {
            foreach (CompartmentRef cR in compartments)
            {
                CompartmentObject compartmentObject = null;
                foreach (CompartmentObject o in restoredCompartments)
                {
                    if (o.GetName().Equals(cR.Name))
                    {
                        compartmentObject = o;
                        SceneCreatorUIController.Get.CompartmentsListView.SelectedIndex = SceneCreatorUIController.Get.CompartmentsListView.Add(compartmentObject.name);
                        break;
                    }
                }
                if (compartmentObject != null)
                {
                    CompartmentObject parentOfThat = null;
                    List<CompartmentObject> childrenOfThat = new List<CompartmentObject>();

                    foreach (CompartmentObject o in restoredCompartments)
                    {
                        if (o.GetName().Equals(cR.Parent))
                        {
                            parentOfThat = o;
                        }
                        if (cR.Children != null && cR.Children.Contains(o.GetName()))
                        {
                            childrenOfThat.Add(o);
                        }
                    }

                    compartmentObject.parent = parentOfThat;
                    compartmentObject.children = childrenOfThat;
                }
            }
        }

        /**
         * Generates a DecisionTree reference object, which contains all necessary information about the current decision tree
         */
        private static DecisionTreeRef GenerateDecisionTreeRef()
        {
            DecisionTreeRef decisionTree = new DecisionTreeRef();
            decisionTree.variances = DecisionTreeManager.Get.variances;
            decisionTree.placedOveralls = DecisionTreeManager.Get.placedOveralls;
            decisionTree.rootNodes = DecisionTreeManager.Get.rootNodes;

            return decisionTree;
        }

        /**
         * Restores the decision tree by the loaded reference.
         */
        private static void RestoreDecisionTree(DecisionTreeRef dTree)
        {
            DecisionTreeManager.Get.variances = dTree.variances;
            DecisionTreeManager.Get.placedOveralls = dTree.placedOveralls;
            DecisionTreeManager.Get.rootNodes = dTree.rootNodes;
        }
    }

    //ObjType identifies the parsed JSON object
    enum ObjType { PROTEIN, MEMBRANE, COMPARTMENT, DECISION_TREE, CLUSTER_SET }

    /**
     * Serialized object, holds the serialized information
     */
    [JsonObject(MemberSerialization.OptIn)]
    class JsonSerializableObj
    {
        [JsonProperty]
        public ObjType ObjType { get; set; }

        [JsonProperty]
        public Protein pObj { get; set; }

        [JsonProperty]
        public CompartmentRef cObj { get; set; }

        [JsonProperty]
        public DecisionTreeRef dTree { get; set; }

        [JsonProperty]
        public Dictionary<int, ProteinCluster> cMan { get; set; }
    }

    /**
     * Protein pseudo-class, used for de-serialization, holds all relevant information
     */
    [JsonObject(MemberSerialization.OptIn)]
    public class Protein
    {
        [JsonProperty]
        public Vector4 Position { get; set; }

        [JsonProperty]
        public Vector4 Rotation { get; set; }

        [JsonProperty]
        public Vector4 Information { get; set; }

        [JsonProperty]
        public Vector4 ReferenceOrientationAxis { get; set; }

        [JsonProperty]
        public Vector4 ReferenceYAxis { get; set; }

        [JsonProperty]
        public Vector4 ReferenceXAxis { get; set; }
    }

    /**
     * Compartment pseudo-class, used for de-serialization, holds all relevant information
     */
    [JsonObject(MemberSerialization.OptIn)]
    class CompartmentRef
    {
        [JsonProperty]
        public CompartmentType CompartmentType { get; set; }

        [JsonProperty]
        public String Parent { get; set; }

        [JsonProperty]
        public List<String> Children { get; set; }

        [JsonProperty]
        public int MembraneId { get; set; }

        [JsonProperty]
        public int MembraneAmount { get; set; }

        [JsonProperty]
        public Vector3 RadiiEllipsoid { get; set; }

        [JsonProperty]
        public Vector3 RadiiRing { get; set; }

        [JsonProperty]
        public Vector3 CapsuleA { get; set; }

        [JsonProperty]
        public Vector3 CapsuleB { get; set; }

        [JsonProperty]
        public Vector3 Center { get; set; }

        [JsonProperty]
        public Vector3 BbMinX { get; set; }

        [JsonProperty]
        public Vector3 BbMinY { get; set; }

        [JsonProperty]
        public Vector3 BbMinZ { get; set; }

        [JsonProperty]
        public Vector3 BbMaxX { get; set; }

        [JsonProperty]
        public Vector3 BbMaxY { get; set; }

        [JsonProperty]
        public Vector3 BbMaxZ { get; set; }

        [JsonProperty]
        public String Name { get; set; }
    }

    /**
     * DecisionTree pseudo-class, used for de-serialization, holds all relevant information
     */
    [JsonObject(MemberSerialization.OptIn)]
    class DecisionTreeRef
    {
        [JsonProperty]
        public Dictionary<int, float> variances { get; set; }

        [JsonProperty]
        public Dictionary<int, TreeNode> rootNodes { get; set; }

        [JsonProperty]
        public Dictionary<int, int> placedOveralls { get; set; }
    }
}
