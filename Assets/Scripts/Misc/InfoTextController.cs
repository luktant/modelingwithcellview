﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using SimpleJSON;
using System.IO;

[ExecuteInEditMode]
public class InfoTextController : MonoBehaviour
{
    private static readonly string CfgFileName = "ingredients-config.json";

    public RawImage InfoTextParent;

    private Dictionary<string, string> infoTexts;
    private Dictionary<string, string> infoNames;

    private Dictionary<string, float> ingredientColors;

    private GameObject Heading = null;
    private GameObject Body = null;

    static int counter = 0;

    private static InfoTextController _instance = null;
    public static InfoTextController Get
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<InfoTextController>();
                if (_instance == null)
                {
                    var go = GameObject.Find("_InfoTextController");
                    if (go != null)
                        DestroyImmediate(go);

                    go = new GameObject("_InfoTextController"); //{ hideFlags = HideFlags.HideInInspector };
                    _instance = go.AddComponent<InfoTextController>();
                }
            }
            return _instance;
        }
    }

    public void OnEnable()
    {
        if (Heading == null || Body == null)
        {
            foreach (Transform child in InfoTextParent.transform) // this is a very stupid way to get children of a parent GameObject...
            {
                if (child.name == "Heading") Heading = child.gameObject;
                if (child.name == "Body") Body = child.gameObject;
            }
        }

        LoadInfosAndNames();
        //HideInfo();
    }

    public void Start()
    {
        HideInfo();
    }

    private void LoadInfosAndNames()
    {
        infoTexts = new Dictionary<string, string>();
        infoNames = new Dictionary<string, string>();

        string strContent = "";
        try
        {
            strContent = File.ReadAllText(Application.dataPath + "/Resources/compartment-descriptions.json");
        }
        catch
        {
            Debug.LogError("Could NOT load up description file!");
            return;
        }


        var content = JSON.Parse(strContent);
        Debug.Log(content["key"]["description"]);

        foreach (var key in content.GetAllKeys())
        {
            infoTexts.Add(key, content[key]["descr"]);
            infoNames.Add(key, content[key]["name"]);
        }
    }

    /**
     * Returns the full ingredient name
     */
    public string GetIngredientNameForPdbName(string pdbName)
    {
        string name = pdbName.ToLower();
        if (infoNames.ContainsKey(name)) return infoNames[name];
        counter++;
        return pdbName;
    }

    /**
     * Returns the hue for a certain pdb file, stored in @CfgFileName
     */
    public float GetIngredientHueForPdbName(string pdbName)
    {
        //return UnityEngine.Random.Range(0.0f, 1.0f);
        if (ingredientColors.ContainsKey(pdbName.ToLower())) return ingredientColors[pdbName.ToLower()];
        return 0;
    }

    /**
     * Loads information from @CfgFileName
     * Info contains full ingredient name, description and a color value, file can be manipulated during runtime
     */
    public void LoadIngredientInformation()
    {
        infoTexts = new Dictionary<string, string>();
        infoNames = new Dictionary<string, string>();
        ingredientColors = new Dictionary<string, float>();

        string strContent = "";

        try
        {
            strContent = File.ReadAllText(Application.dataPath + "/Resources/" + CfgFileName); // ReadAllText vs ReadAllLines????
        }
        catch
        {
            Debug.LogError("Could NOT load up description file, creating new one");
            strContent += "{\n";

            foreach (string name in SceneCreatorUIController.Get.GetAllIngredientNames()) strContent += ConvertIngredientToJSON(name) + "\n";
            strContent += "\n}";
            File.WriteAllText(Application.dataPath + "/Resources/"+CfgFileName, strContent);
        }

        var content = JSON.Parse(strContent);

        foreach (var key in content.GetAllKeys())
        {
            infoTexts.Add(key.ToLower(), content[key]["descr"]);
            infoNames.Add(key.ToLower(), content[key]["name"]);

            float color;
            float.TryParse(content[key]["hue"], out color);
            ingredientColors.Add(key.ToLower(), color);
        }

        if (ingredientColors.Count > 0) ColorManager.Get.ReloadColors();
    }

    /**
     * If not present, a newly downloaded ingredient is added to @CfgFileName
     */
    public void InsertCustomIngredient(string name)
    {
        if (!infoTexts.ContainsKey(name.ToLower()))
        {
            string json = File.ReadAllText(Application.dataPath + "/Resources/"+CfgFileName);
            json = json.Substring(0, json.LastIndexOf("}"));
            json += ConvertIngredientToJSON(name);
            json += "\n}";
            File.WriteAllText(Application.dataPath + "/Resources/"+CfgFileName, json);
        }
    }

    /**
     * Hardcoded because SimpleJSON is inconvinient. Works as well
     */
    public string ConvertIngredientToJSON(string name)
    {
        return "\t\"" + name + "\"" + ":{\n\t\t\"name\":\"" + name + "\",\n\t\t\"descr\":\"No description available\",\n\t\t\"hue\":\""+ UnityEngine.Random.Range(0, 360) +"\"\n\t}";
    }

    public void ShowInfoFor(string path)
    {
        LookUpInfoText(path);
        SetInfoTextVisibility(SceneCreatorUIController.Get.showInfo.isOn?true:false);
        //AdjustSizeAndPosition(); // not needed, I accomplished it by using UI tools
    }

    public void ShowInfoForInstance(int selectedInstanceId)
    {
        if (selectedInstanceId == -123)         // RNA
        {
            ShowInfoFor("root.interior2_HIV_RNA");
        }
        else if (selectedInstanceId >= 100000)  // Lipid
        {
            // there are only two lipid types so it should be fairly easy
            var ingrId = CPUBuffers.Get.LipidInstanceInfos[selectedInstanceId - 100000].x;
            //Debug.Log("ShowInfoForInstance: Lipid, id = " + ingrId);
            var ingredientPath = SceneManager.Get.AllIngredientNames[(int)ingrId];
            //Debug.Log("ShowInfoFor path = " + ingredientPath);
            ShowInfoFor(ingredientPath);
        }
        else if (selectedInstanceId >= 0)       // Protein
        {
            if (GlobalProperties.Get.CreateOrEditMode)
            {
                string name = SceneCreatorUIController.Get.GetIngredientNameById((int)CPUBuffers.Get.ProteinInstanceInfos[selectedInstanceId].x).ToLower();

                var nameToShow = "Dummy Name";
                var descriptionToShow = "Dummy Description";

                if (infoNames.ContainsKey(name)) nameToShow = infoNames[name];
                if (infoTexts.ContainsKey(name)) descriptionToShow = infoTexts[name];

                var headingText = Heading.GetComponent<Text>();
                headingText.text = nameToShow;
                var bodyText = Body.GetComponent<Text>();
                bodyText.text = descriptionToShow;
                SetInfoTextVisibility(true);
            }
            else
            {
                var ingrId = CPUBuffers.Get.ProteinInstanceInfos[selectedInstanceId].x;
                //Debug.Log("ShowInfoForInstance: Protein, id = " + ingrId);
                var ingredientPath = SceneManager.Get.ProteinIngredientNames[(int)ingrId];
                ShowInfoFor(ingredientPath);
            }
        }
        else                                    // Nothing selected (-1)
        {
            HideInfo();
            return;
        }

        //var ingredientId = CPUBuffers.Get.ProteinInstanceInfos[selectedInstanceId];

    }

    public void HideInfo()
    {
        SetInfoTextVisibility(false);
    }

    private void AdjustSizeAndPosition()
    {
        // TODO:
    }

    /// <summary>
    /// Looks up text to show for a certain compartment or protein type
    /// </summary>
    /// <param name="path">hierarchy path to the element that we want to set into text for</param>
    private void LookUpInfoText(string path)
    {
        var nameToShow = "Dummy Name";
        var descriptionToShow = "Dummy Description";

        UnityEngine.Debug.Log(path);

        if (infoNames.ContainsKey(path)) nameToShow = infoNames[path];
        if (infoTexts.ContainsKey(path)) descriptionToShow = infoTexts[path];

        var headingText = Heading.GetComponent<Text>();
        headingText.text = nameToShow;
        var bodyText = Body.GetComponent<Text>();
        bodyText.text = descriptionToShow;
    }

    /// <summary>
    /// Method for showing and hiding info text area
    /// </summary>
    public void SetInfoTextVisibility(bool visible)
    {
        //GameObject heading = null;
        //GameObject body = null;
        //foreach (Transform child in InfoTextParent.transform) // this is a very stupid way to get children of a parent GameObject...
        //{
        //    if (child.name == "Heading") heading = child.gameObject;
        //    if (child.name == "Body") body = child.gameObject;
        //}

        if (SceneCreatorUIController.Get.showInfo.isOn && visible)
        {
            var bg = InfoTextParent.GetComponent<RawImage>();
            bg.color = new Color(bg.color.r, bg.color.g, bg.color.b, 0.235f);
            var elem = Heading.GetComponent<Text>();
            elem.color = new Color(elem.color.r, elem.color.g, elem.color.b, 1.0f);
            elem = Body.GetComponent<Text>();
            elem.color = new Color(elem.color.r, elem.color.g, elem.color.b, 1.0f);
        }
        else
        {
            var bg = InfoTextParent.GetComponent<RawImage>();
            bg.color = new Color(bg.color.r, bg.color.g, bg.color.b, 0.0f);
            var elem = Heading.GetComponent<Text>();
            elem.color = new Color(elem.color.r, elem.color.g, elem.color.b, 0.0f);
            elem = Body.GetComponent<Text>();
            elem.color = new Color(elem.color.r, elem.color.g, elem.color.b, 0.0f);
        }
    }

    // DEBUG!!!!!!!!!!!!!!!!!!!!!!!!!
    public static void GenerateJSON()
    {
        var hierarchy = SceneManager.Get.SceneHierarchy;
        var path = Application.dataPath + "/Resources/compartment-descriptions.json";
        string jsonOutput = "{";

        foreach (var ingredientPath in hierarchy)
        {
            jsonOutput += "\"" + ingredientPath + "\"" + ":{";
            jsonOutput += "\"name\":\"Some Name\",\"descr\":\"This is a description\"}";

            if (hierarchy.IndexOf(ingredientPath) != hierarchy.Count - 1) jsonOutput += ",";
        }

        jsonOutput += "}";
        File.WriteAllText(path, jsonOutput);
    }
}
