﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Assets.Scripts.Loaders;
using Assets.Scripts.Misc;

/**
 * The torus compartment is almost working, one problem is the distance function, which has to be adapted with 3D radii to allow scaling. It was removed from the UI
 * List because it makes more problems than it is useful, never saw a microorganism in the shape of a doughnut
 */
public class TorusCompartment : CompartmentObject
{
    protected string NAME = "Torus";
    private static int ID = 0;

    //radiiRing is the radius of the whole torus
    //radiiShere is the radius of the torus ring sphere
    public Vector3 radiiRing = new Vector3(40.0f, 40.0f, 40.0f);
    public Vector3 radiiSphere = new Vector3(10.0f, 10.0f, 10.0f);

    public TorusCompartment() { }

    void Start()
    {
        lastScale = transform.localScale;
        compartmentType = CompartmentType.Torus;
    }

    /**
     * Handles tranforms for translate and scale, for translate multiply is false, otherwise true
     */
    protected override void AdaptCompartmentToTransform(Vector4 transform, HandleSelectionState state)
    {        
        if (state == HandleSelectionState.Scale)
        {
            lastScale = handle.transform.localScale;
            radiiRing = new Vector3(radiiRing.x * transform.x, radiiRing.y * transform.y, radiiRing.z * transform.z);
            radiiSphere = new Vector3(radiiSphere.x * transform.x, radiiSphere.y * transform.y, radiiSphere.z * transform.z);
        }
        else if (state == HandleSelectionState.Translate)
        {
            center = new Vector3(center.x + transform.x, center.y + transform.y, center.z + transform.z);
            lastPosition = handle.transform.localPosition;
        }
        else
        {
            lastRotation = transform;
            center = new Vector4((boundingBox.maxX.x + boundingBox.minX.x) / 2, (boundingBox.maxY.y + boundingBox.minY.y) / 2, (boundingBox.maxZ.z + boundingBox.minZ.z) / 2, membraneId);
            handle.transform.position = center;
        }
    }


    public override float GetSignedDistance(Vector3 point)
    {
        Vector3 p = point - center;
        float r1 = radiiRing.x, r2 = radiiSphere.x;       //Meanwhile only use equally scaled torus

        Vector2 q = new Vector2((float)Math.Sqrt(Math.Pow(p.x, 2) + Math.Pow(p.z, 2)) - r1, p.y);
        return (float)Math.Sqrt(Math.Pow(q.x, 2) + Math.Pow(q.y, 2)) - r2;

        //TODO Use correct distance function with maybe ellipsoid by translating the position by radius?
        /*
        Vector3 pAbs = new Vector3(Math.Abs(p.x), Math.Abs(p.y), Math.Abs(p.z));
        Vector3 toCenter = new Vector3(Math.Abs(radiiRing.x), Math.Abs(radiiRing.y), Math.Abs(radiiRing.z));
        Vector3 toRad = new Vector3(Math.Abs(radiiSphere.x), Math.Abs(radiiSphere.y), Math.Abs(radiiSphere.z))*2;

        return GetSignedDistanceEllipsoid(pAbs-toCenter-toRad);*/
    }
    /*
    public float GetSignedDistanceEllipsoid(Vector3 position)
    {
        return (float)(Vector3.Scale(position, new Vector3(1 / radiiSphere.x, 1 / radiiSphere.y, 1 / radiiSphere.z)).magnitude - 1.0) * Mathf.Min(Mathf.Min(radiiSphere.x, radiiSphere.y), radiiSphere.z);
    }
     */

    public override void HalfSize()
    {
        Vector3 parentSize = parent.GetBoundsMax() - parent.GetBoundsMin();
        radiiRing = parentSize / 4.0f;
        radiiSphere = parentSize / 4.0f;

        boundingBox.maxX = parent.GetBoundsMax() / 4.0f;
        boundingBox.maxY = parent.GetBoundsMax() / 4.0f;
        boundingBox.maxZ = parent.GetBoundsMax() / 4.0f;
        boundingBox.minX = parent.GetBoundsMin() / 4.0f;
        boundingBox.minY = parent.GetBoundsMin() / 4.0f;
        boundingBox.minZ = parent.GetBoundsMin() / 4.0f;
    }

    protected override string GetName(bool increment)
    {
        if (increment) ID++;
        return NAME + ID;
    }

    protected override Vector3 GetPossibleChildOrigin()
    {
        //TODO care about deformed torus
        Vector3 pos = center;
        pos.x = pos.x - radiiRing.x - radiiSphere.x;
        return pos;
    }
}
