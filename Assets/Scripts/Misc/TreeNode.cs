﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Utils;
using Assets.Scripts.Misc;
using System;
using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn)]
public class TreeNode
{
    [JsonProperty]
    public int ID = 0;

    [JsonProperty]
    public List<TreeNode> childNodes = new List<TreeNode>();

    [JsonProperty]
    private int clusterId = -1;

    [JsonProperty]
    private int nodeID;

    [JsonProperty]
    public float placedInside = 0;

    [JsonProperty]
    private float placedOnMembran = 0;

    [JsonProperty]
    public List<RotationProbabilityPair> rotationPropabilities = new List<RotationProbabilityPair>();

    [JsonProperty]
    public float rotated = 0;

    [JsonProperty]
    private bool isRoot;

    [JsonProperty]
    public string compObjName;
    //Serializing this separatly, had to be handlet differently
    public CompartmentObject compObj;

    public Dictionary<Quaternion, int> samples = new Dictionary<Quaternion, int>();

    public float sampleAmount = 0;

    public TreeNode(CompartmentObject cO, bool isRoot, int proteinID)
    {
        ID = proteinID;

        if (cO != null)
        {
            this.compObj = cO;
            this.compObjName = cO.name;

            List<CompartmentObject> children = cO.GetChildren();
            foreach (CompartmentObject c in children)
            {
                TreeNode childNode = new TreeNode(c, false, proteinID);
                childNodes.Add(childNode);
            }
        }
        this.isRoot = isRoot;
    }

    /**
     * Checks recursive for the given position if it is inside the current tree-node's compartment. If the smallest container
     * compartment is found, the position (inside, on surface, outside) is calculated and the probabilities are adjusted.
     */
    public bool ModifyTreeNodePositions(Vector3 position)
    {
        bool check = false;
        if (isRoot)
        {
            foreach (TreeNode child in childNodes)
            {
                check = check || child.ModifyTreeNodePositions(position);
            }
            if (check == false)
            {
                placedInside++;
            }
            return check;
        }
        else
        {
            // checks if the point is inside or outside of the compartment 
            float inOrOut = compObj.GetSignedDistance(position);
            // if it is negative it is inside 
            if (CalculationUtility.AlmostEquals(inOrOut, 0, 5f))
            {
                placedOnMembran++;
                return true;
            }
            else if (inOrOut < 0)
            {
                //if the compartment has no childs chnage the probability  
                if (childNodes.Count == 0)
                {
                    placedInside++;
                    return true;
                }
                else
                {
                    foreach (TreeNode child in childNodes)
                    {
                        check = check || child.ModifyTreeNodePositions(position);
                    }

                    if (check == false)
                    {
                        placedInside++;
                        return true;
                    }
                    return check;
                }
            }
            else
            {
                return false;
            }
        }
    }

    /**
     * Inverse operation for the ModifyTreeNodePositions method, removes all set variables and restores the previous
     * probabilities. When modifications are performed in the DecisionTreeManager both the modification and the respective 
     * inverse method are returned as an action pair for redo and undo operations.
     */
    public bool UndoModifyTreeNodePositions(Vector3 position)
    {
        bool check = false;
        if (isRoot)
        {
            foreach (TreeNode child in childNodes)
            {
                check = check || child.UndoModifyTreeNodePositions(position);
            }
            if (check == false)
            {
                placedInside--;
            }
            return check;
        }
        else
        {
            // checks if the point is inside or outside of the compartment 
            float inOrOut = compObj.GetSignedDistance(position);
            // if it is negative it is inside 
            if (CalculationUtility.AlmostEquals(inOrOut, 0, 5f))
            {
                placedOnMembran--;
                return true;
            }
            else if (inOrOut < 0)
            {
                //if the compartment has no childs chnage the probability  
                if (childNodes.Count == 0)
                {
                    placedInside--;
                    return true;
                }
                else
                {
                    foreach (TreeNode child in childNodes)
                    {
                        check = check || child.ModifyTreeNodePositions(position);
                    }

                    if (check == false)
                    {
                        placedInside--;
                        return true;
                    }
                    return check;
                }
            }
            else
            {
                return false;
            }
        }
    }

    /**
     * Checks recursive for the given rotation and position if it is inside the current tree-node's compartment. If the smallest container
     * compartment is found, the rotation probabilities are calculated and adjusted.
     */
    public bool ModifyTreeNodeRotationProbabilities(Vector4 position, Quaternion rotation)
    {
        bool check = false;
        if (isRoot)
        {
            foreach (TreeNode child in childNodes)
            {
                check = check || child.ModifyTreeNodeRotationProbabilities(position, rotation);
            }
            if (check == false)
            {
                ComputeNewRotationPropabilities(position, rotation, false, true);
            }
            return check;
        }
        else
        {
            // checks if the point is inside or outside of the compartment 
            float inOrOut = compObj.GetSignedDistance(position);
            // if it is negative it is inside 
            if (CalculationUtility.AlmostEquals(inOrOut, 0, 5f))
            {
                ComputeNewRotationPropabilities(position, rotation, true, true);
                return true;
            }
            else if (inOrOut < 0)
            {
                //if the compartment has no childs chnage the probability  
                if (childNodes.Count == 0)
                {
                    ComputeNewRotationPropabilities(position, rotation, false, true);
                    return true;
                }
                else
                {
                    foreach (TreeNode child in childNodes)
                    {
                        check = check || child.ModifyTreeNodeRotationProbabilities(position, rotation);
                    }

                    if (check == false)
                    {
                        ComputeNewRotationPropabilities(position, rotation, false, true);
                        return true;
                    }
                    return check;
                }
            }
            else
            {
                return false;
            }
        }
    }

    /**
     * Inverse operation for the ModifyTreeNodeRotationProbabilities method, removes all set variables and restores the previous
     * probabilities. When modifications are performed in the DecisionTreeManager both the modification and the respective 
     * inverse method are returned as an action pair for redo and undo operations.
     */
    public bool UndoModifyTreeNodeRotationProbabilities(Vector4 position, Quaternion rotation)
    {
        bool check = false;
        if (isRoot)
        {
            foreach (TreeNode child in childNodes)
            {
                check = check || child.UndoModifyTreeNodeRotationProbabilities(position, rotation);
            }
            if (check == false)
            {
                int index = rotationPropabilities.Count - 1;
                RotationProbabilityPair currentRotationPair = rotationPropabilities[index];

                RemoveLastSample(currentRotationPair.GetReferenceNormalVector(), currentRotationPair.GetRotation());

                if (rotationPropabilities[index].GetRotated() <= 1)
                {
                    rotationPropabilities.RemoveAt(index);
                }
                else
                {
                    rotationPropabilities[index].DecrementRotated();
                }
            }
            return check;
        }
        else
        {
            // checks if the point is inside or outside of the compartment 
            float inOrOut = compObj.GetSignedDistance(position);
            // if it is negative it is inside 
            if (CalculationUtility.AlmostEquals(inOrOut, 0, 5f))
            {
                int index = rotationPropabilities.Count - 1;
                if (rotationPropabilities[index].GetRotated() <= 1)
                {
                    rotationPropabilities.RemoveAt(index);
                }
                else
                {
                    rotationPropabilities[index].DecrementRotated();
                }
                return true;
            }
            else if (inOrOut < 0)
            {
                if (childNodes.Count == 0)
                {
                    int index = rotationPropabilities.Count - 1;
                    RotationProbabilityPair currentRotationPair = rotationPropabilities[index];

                    RemoveLastSample(currentRotationPair.GetReferenceNormalVector(), currentRotationPair.GetRotation());

                    if (rotationPropabilities[index].GetRotated() <= 1)
                    {
                        rotationPropabilities.RemoveAt(index);
                    }
                    else
                    {
                        rotationPropabilities[index].DecrementRotated();
                    }
                    return true;
                }
                else
                {
                    foreach (TreeNode child in childNodes)
                    {
                        check = check || child.ModifyTreeNodeRotationProbabilities(position, rotation);
                    }

                    if (check == false)
                    {
                        int index = rotationPropabilities.Count - 1;
                        RotationProbabilityPair currentRotationPair = rotationPropabilities[index];

                        RemoveLastSample(currentRotationPair.GetReferenceNormalVector(), currentRotationPair.GetRotation());

                        if (rotationPropabilities[index].GetRotated() <= 1)
                        {
                            rotationPropabilities.RemoveAt(index);
                        }
                        else
                        {
                            rotationPropabilities[index].DecrementRotated();
                        }
                        return true;
                    }
                    return check;
                }
            }
            else
            {
                return false;
            }
        }
    }

    /**
     * Called after user affected rotation, creates new RotationProbabilityPairs and Samples. Both approaches
     * are included in the method. The RotationProbsbilityPairs can be used for simple rotations. With the samples a kernel 
     * density estimation is build which interpolates between the samples. 
     */
    public void ComputeNewRotationPropabilities(Vector4 position, Quaternion rotation, bool onMembran, bool modifyTree)
    {
        Vector4 rotVec = new Vector4(rotation.eulerAngles.x, rotation.eulerAngles.y, rotation.eulerAngles.z, 0);

        Vector3 proteinCenter = new Vector3(position.x, position.y, position.z);
        if (true)
        {
            // check which surface is the nearest 
            float nearestDistance = 2000f;
            CompartmentObject referenceCompartment = null;
            foreach (TreeNode n in childNodes)
            {
                float aktDistance = n.GetCompartmentObject().GetSignedDistance(position);
                if (aktDistance < nearestDistance)
                {
                    nearestDistance = aktDistance;
                    referenceCompartment = n.GetCompartmentObject();
                }
            }

            RotationProbabilityPair currentRotationPair;

            if (IsClustered())
            {
                Vector3 surfacePoint = CalculationUtility.GetNearestSurfacePoint(ClusterManager.GetClusterForId(clusterId).GetPseudoCompartment(), proteinCenter, Vector3.zero);
                Vector3 normalVector = Vector3.Normalize(CalculationUtility.GetNormalVectorNew(ClusterManager.GetClusterForId(clusterId).GetPseudoCompartment(), surfacePoint));
                Vector3 xAxis = Vector3.Normalize(rotation * new Vector3(1, 0, 0));
                Vector3 yAxis = Vector3.Normalize(rotation * new Vector3(0, 1, 0));

                currentRotationPair = new RotationProbabilityPair(rotation, normalVector, xAxis, yAxis, ClusterManager.GetClusterForId(clusterId).GetPseudoCompartment(), false);
            }
            // case: only one compartment orient the rotation to the center of the compartment
            else if (referenceCompartment == null)
            {
                Vector3 transformVector = Vector3.zero - compObj.GetCenter();

                Vector3 compObjCenter = new Vector3(0, 0, 0);
                Vector3 directionVector = Vector3.Normalize((proteinCenter + transformVector) - compObjCenter);
                Vector3 yAxis = Vector3.Normalize(rotation * new Vector3(0, 1, 0));
                Vector3 xAxis = Vector3.Normalize(rotation * new Vector3(1, 0, 0));

                currentRotationPair = new RotationProbabilityPair(rotation, directionVector, xAxis, yAxis, compObj, false);
            }
            else
            {
                Vector3 surfacePoint = CalculationUtility.GetNearestSurfacePoint(referenceCompartment, proteinCenter, Vector3.zero);
                Vector3 normalVector = Vector3.Normalize(CalculationUtility.GetNormalVectorNew(referenceCompartment, surfacePoint));
                Vector3 xAxis = Vector3.Normalize(rotation * new Vector3(1, 0, 0));
                Vector3 yAxis = Vector3.Normalize(rotation * new Vector3(0, 1, 0));

                currentRotationPair = new RotationProbabilityPair(rotation, normalVector, xAxis, yAxis, referenceCompartment, true);
            }
            if (modifyTree)
            {
                if (IsClustered())
                {
                    currentRotationPair.IncrementRotated();
                    ClusterManager.GetClusterForId(clusterId).AddRotationProbabilityPair(currentRotationPair);
                }
                else
                {
                    bool alreadyExists = false;
                    foreach (RotationProbabilityPair rp in rotationPropabilities)
                    {
                        if (rp.Equals(currentRotationPair))
                        {
                            rp.IncrementRotated();
                            alreadyExists = true;
                            break;
                        }
                    }

                    if (!alreadyExists)
                    {
                        currentRotationPair.IncrementRotated();
                        rotationPropabilities.Add(currentRotationPair);
                    }

                    SetNewSample(currentRotationPair.GetReferenceNormalVector(), currentRotationPair.GetRotation());

                    rotated++;
                }
            }
        }
    }

    /**
     * Returns the placement probability for this node
     */
    public float GetProbability()
    {
        float probability = 0;
        if (DecisionTreeManager.Get.placedOveralls[ID] != 0)
        {
            probability = placedInside / DecisionTreeManager.Get.placedOveralls[ID];
        }
        return probability;
    }

    /**
     * Returns the probability that the protein is on the membrane, if membrane ingredient the probability is 1.0
     */
    public float GetProbabilityForMembran(bool membraneProtein)
    {
        float probability = 0;

        if (membraneProtein)
        {
            return 1.0f;
        }
        else
        {
            if (DecisionTreeManager.Get.placedOveralls[ID] != 0)
            {
                probability = placedOnMembran / DecisionTreeManager.Get.placedOveralls[ID];
            }
        }
        return probability;
    }

    public List<RotationProbabilityPair> GetRotationProbability()
    {
        if (!IsClustered()) return rotationPropabilities;
        else return ClusterManager.GetClusterForId(clusterId).GetRotationProbabilities();
    }

    /**
     * Adds a child node to the current tree node
     */
    public void AddChildNode(TreeNode child)
    {
        childNodes.Add(child);
    }

    /**
     * Recursive, returns all child nodes 
     */
    public List<TreeNode> GetNodes()
    {
        List<TreeNode> nodeList = new List<TreeNode>();

        foreach (TreeNode tn in childNodes)
        {
            nodeList.AddRange(tn.GetNodes());
            nodeList.Add(tn);
        }
        return nodeList;
    }

    /**
     * Increments amount of placed ingredients, the decision tree manager holds the information about the total amount of placed ones 
     */
    public void IncrementPlacedOverall()
    {
        DecisionTreeManager.Get.placedOveralls[ID]++;
    }

    /**
     * Inverse method of the IncrementPlacedOverall method, decrements amount of placed ingredients
     */
    public void DecrementPlacedOverall()
    {
        DecisionTreeManager.Get.placedOveralls[ID]--;
    }

    /**
     * Returns this tree node's compartment object
     */
    public CompartmentObject GetCompartmentObject()
    {
        return compObj;
    }

    /**
     * Returns whether the ingredients in this tree node are clustered. Currently they can only be clustered or not, not both
     */
    public bool IsClustered()
    {
        return clusterId >= 0;
    }

    /**
     * Returns the cluster id of this node, the ID is used to identify the cluster information in ClusterManger. This is needed
     * because clusters may be formed with different ingredients too (and thus also different treenodes), workaround for shared properties.
     * If no cluster exists -1 is returned, IsClustered should be called first
     */
    public int GetClusterId()
    {
        return clusterId;
    }

    /**
     * Sets the cluster id for this tree node, for later identification (shared properties between 
     */
    public void SetClusterId(int id)
    {
        UnityEngine.Debug.Log("Setting cluster for ingredient " + ID + " with id " + id + " for compartment " + GetCompartmentObject().GetName());
        clusterId = id;
        foreach (RotationProbabilityPair pair in rotationPropabilities) ClusterManager.GetClusterForId(id).AddRotationProbabilityPair(pair);
    }

    /**
     * Returns the cluster for this treenode, which is identified by the clusterId.
     */
    public List<CustomTriple<int, Vector4, Quaternion>> GetClusterPosAndRot()
    {
        return ClusterManager.GetClusterForId(clusterId).GetCluster();
    }

    /*
     * Invoked after user affected rotation. 
     * @rotation rotation quaternion belonging to the user rotated protein
     * @normalVec normal vector belonging to the user rotated  protein
     * 
     * Transforms the quaternion into reference space. 
     */
    public void SetNewSample(Vector3 normalVec, Quaternion rotation)
    {
        Quaternion rotToReferenceSpace = Quaternion.FromToRotation(normalVec, new Vector3(0, 1, 0));
        Quaternion sample =  rotToReferenceSpace* rotation ;
        if (samples.ContainsKey(sample))
        {
            samples[sample]++;
        }
        else
        {
            samples.Add(sample, 1);
        }
        sampleAmount++;
    }

    /*
     * Inverse method to  SetNewSample.
     * Removes the last set sample quaternion from the list and decrements the amount of the samples.  
     * The method to undo the last user rotation.
     */
    public void RemoveLastSample(Vector3 normalVec, Quaternion rotation)
    {
        Quaternion rotToReferenceSpace = Quaternion.FromToRotation(normalVec, new Vector3(0, 1, 0));
        Quaternion sample = rotation * rotToReferenceSpace;
        if (samples.ContainsKey(sample))
        {
            samples[sample]--;
            if (samples[sample] <= 0)
            {
                samples.Remove(sample);
            }
        }
        sampleAmount--;
    }

    /*
     * Returns the List with the stored Quaternions in reference space, needed for the calculation of the random Quaternions.
     */
    public Dictionary<Quaternion, int> GetSamples()
    {
        return samples;
    }
    /*
     * Returns a random quaternion, which is evaluated by a kernel density function with a gauß kernel. 
     */ 
    public Quaternion GetRandomKDESample()
    {

        Quaternion randQ = Quaternion.identity;
        bool validQuaternion = false;

        float n = sampleAmount;

        float probability = 0;
        float sigma = ((2 * Mathf.PI) / 360) * DecisionTreeManager.Get.GetIngredientVariance(ID);
        if (sigma < 0) sigma *= (-1.0f);
        float factor = 1 / (n * 0.5f * sigma * Mathf.Sqrt(2 * Mathf.PI));

        float addend = 0;
        float exponent = 0;
        Quaternion curQ = Quaternion.identity;
        int amount = 0;
        int counter = 0;
        while (!validQuaternion && counter < 1500)
        {
            float sum = 0;
            probability = 0;
            randQ = Quaternion.AngleAxis(UnityEngine.Random.Range(0, 360), new Vector3(UnityEngine.Random.Range(-1.0f, 1.0f), UnityEngine.Random.Range(-1.0f, 1.0f), UnityEngine.Random.Range(-1.0f, 1.0f)));

            foreach (KeyValuePair<Quaternion, int> kvpair in samples)
            {
                curQ = kvpair.Key;
                amount = kvpair.Value;

                /*Vector3 v = MyUtility.QuanternionToVector4(randQ) - MyUtility.QuanternionToVector4(curQ);
                addend1 = Vector3.Dot(v, v);

                addend1 = Mathf.Exp((-1 / (2 * Mathf.Pow(sigma, 2))) * addend1);

                sum1 += amount * addend1;*/

                exponent = (-1 / (2 * Mathf.Pow(sigma, 2))) * (Mathf.Pow(randQ.x - curQ.x, 2) +
                                                               Mathf.Pow(randQ.y - curQ.y, 2) +
                                                               Mathf.Pow(randQ.z - curQ.z, 2) +
                                                               Mathf.Pow(randQ.w - curQ.w, 2));
                addend = Mathf.Exp(exponent);
                sum += addend * amount;


            }

            probability = factor * sum;
            float rand = UnityEngine.Random.Range(0.1f, 1.0f);
            if (rand <= probability && probability <= 1)
            {
                validQuaternion = true;
            }
            counter++;
        }

        return randQ;
    }
}