﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Assets.Scripts.Loaders;
using Assets.Scripts.Misc;
using Newtonsoft.Json;

/**
 * Pseudocompartment object is a pseudocompartment, used especially for clusters. It basically represents an elipsoid, provides the
 * same distance function. This pseudocompartment is never rendered, it is used only as a helper class for the calculation functionality
 * because they work with compartment objects and distance functions.
 */
[JsonObject(MemberSerialization.OptIn)]
public class PseudoCompartment : CompartmentObject
{
    protected string NAME = "Pseudo";
    [JsonProperty]
    private static int ID = 0;
    [JsonProperty]    
    public float radius = 0.0f;

    // Use this for initialization
    void Start()
    {
    }

    /**
     * Handles tranforms for translate scale and rotation, bounding box is updated in compartmentobject
     */
    protected override void AdaptCompartmentToTransform(Vector4 transform, HandleSelectionState state)
    {        
    }

    /**
     * Uses the signed distance of the sphere
     */
    public override float GetSignedDistance(Vector3 point)
    {
        Vector3 position = point - center;
        return (float)(Vector3.Scale(position, new Vector3(1 / radius, 1 / radius, 1 / radius)).magnitude - 1.0) * radius;
    }
        
    /**
     * Not needed for pseudocompartment
     */
    public override void HalfSize()
    {
    }

    /**
     * Should not be used, not needed for pseudocompartment
     */
    protected override string GetName(bool increment)
    {
        return NAME + ID;
    }

    protected override Vector3 GetPossibleChildOrigin()
    {
        return center;
    }
}
