using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Loaders;
using Assets.Scripts.Misc;
using Assets.Scripts.Utils;

/**
 * Definition of different compartment types, ellipsoid and capsule are the only used ones,
 * torus exists but is not fully working.
 */
public enum CompartmentType
{
    Ellipsoid,
    Capsule,
    Torus
}

/**
 * The compartment object is a core component of the creation tool. Its an abstract class, which provides as much functionality as possible for
 * all deriving compartments. It has declared abstract methods, which have to be implemented in deriving classes, if correct the new compartment
 * can be used properly in code.
 */
public abstract class CompartmentObject : MonoBehaviour
{
    //Compartment membrane vars
    public float MEMBRANE_MAX = 3000.0f;
    protected static string SUB = "Sub";

    public int membraneAmount = 1000;
    public int membraneId;

    public List<Protein> membraneTemplate = null;

    //Object identifiers
    public CompartmentType compartmentType;

    //Parent and children
    public CompartmentObject parent;
    public List<CompartmentObject> children = new List<CompartmentObject>();

    //Store the latest values, to prevent unnecessary updates
    public Vector3 center = Vector3.zero;
    public Vector3 lastPosition = Vector3.zero;
    public Vector3 lastScale = Vector4.one;
    public Vector4 lastRotation;

    protected TransformHandle handle;

    public BoundingBox boundingBox;
    protected bool boundingBoxInitialized = false;

    public struct BoundingBox
    {
        public Vector3 maxX, maxY, maxZ, minX, minY, minZ;
    }

    /**
     * Adds a transform handle to the created game object, set the bounding box initially to zero
     */
    void Awake()
    {
        boundingBox.maxX = Vector3.zero;
        boundingBox.maxY = Vector3.zero;
        boundingBox.maxZ = Vector3.zero;
        boundingBox.minX = Vector3.zero;
        boundingBox.minY = Vector3.zero;
        boundingBox.minZ = Vector3.zero;

        handle = gameObject.AddComponent<TransformHandle>();
    }

    /**
     * The update method handles the transformations (rotation, translation and scale) only if the 
     * handle is enabled and the values have changed since the last frame.
     */
    void Update()
    {
        if (handle.IsEnabled())
        {
            BoundingBox oldBB = boundingBox;

            if (handle.GetSelectionState() == HandleSelectionState.Scale && lastScale != handle.transform.localScale)
            {
                Vector4 scale = new Vector4(1 + handle.transform.localScale.x - lastScale.x, 1 + handle.transform.localScale.y - lastScale.y, 1 + handle.transform.localScale.z - lastScale.z, 1);
                if (scale.x != 1.0f || scale.y != 1.0f || scale.z != 1.0f)
                {
                    ModifyBoundingBox(HandleSelectionState.Scale, scale);

                    if (((Mathf.Max(scale.x, scale.y, scale.z) > 1.0f) && CheckParentBoundsSatisfied()) || ((Mathf.Min(scale.x, scale.y, scale.z) < 1.0f) && CheckChildrenBoundsSatisfied()))
                    {
                        ApplyTransform(scale, HandleSelectionState.Scale);
                    }
                    else
                    {
                        handle.transform.localScale = lastScale;
                        boundingBox = oldBB;
                    }
                }
            }
            else if (handle.GetSelectionState() == HandleSelectionState.Translate && handle.transform.localPosition != lastPosition)
            {
                Vector4 translation = handle.transform.localPosition - lastPosition;
                if (translation.x != 0.0f || translation.y != 0.0f || translation.z != 0.0f)
                {
                    ModifyBoundingBox(HandleSelectionState.Translate, translation);
                    if (CheckParentBoundsSatisfied() && CheckChildrenBoundsSatisfied())
                    {
                        ApplyTransform(translation, HandleSelectionState.Translate);
                    }
                    else
                    {
                        handle.transform.localPosition = lastPosition;
                        boundingBox = oldBB;
                    }
                }
            }
            else if (handle.GetSelectionState() == HandleSelectionState.Rotate && handle.transform.rotation != MyUtility.Vector4ToQuaternion(lastRotation))
            {
                UnityEngine.Debug.Log("Updating bounding box rotate");
                Vector4 rotation = MyUtility.QuanternionToVector4(handle.transform.rotation);

                ModifyBoundingBox(HandleSelectionState.Rotate, Vector3.zero);
                if (CheckParentBoundsSatisfied() && CheckChildrenBoundsSatisfied())
                {
                    ApplyTransform(rotation, HandleSelectionState.Rotate);
                }
                else
                {
                    handle.transform.rotation = MyUtility.Vector4ToQuaternion(lastRotation);
                    boundingBox = oldBB;
                }
            }
        }
    }

    /**
     * Checks if the parent bounds are satisified. Returns whether there are collisions with the parent compartment or not
     */
    protected bool CheckParentBoundsSatisfied()
    {
        if (parent == null && children.Count == 0) return true;
        return parent == null || CheckBoundsForCompartment(parent);
    }

    /**
     * Checks if the childrens bounds are satisified. Returns whether there are collisions with the child compartment(s) or not
     */
    protected bool CheckChildrenBoundsSatisfied()
    {
        bool satisfied = false;
        foreach (CompartmentObject obj in children)
        {
            satisfied = obj.CheckBoundsForCompartment(obj.GetParent());
            if (!satisfied) return false;
        }
        return true;
    }

    /**
     * This method is called from within the update. This method applies the currently selected transformation to every position of the current compartment.
     * The method is only called, if no collisions occur, thus there is no need to check for them and transforms can be easily applied.
     */
    private void ApplyTransform(Vector4 transform, HandleSelectionState state)
    {
        for (int i = 0; i < CPUBuffers.Get.ProteinInstancePositions.Count; i++)
        {
            if (CPUBuffers.Get.ProteinInstancePositions[i].w == membraneId)
            {
                Vector4 oldPos = CPUBuffers.Get.ProteinInstancePositions[i] * GlobalProperties.Get.Scale;
                Vector4 newPos;

                switch (state)
                {
                    case HandleSelectionState.Scale:
                        if (lastRotation != Vector4.zero) oldPos = MyUtility.QuaternionTransform(Quaternion.Inverse(MyUtility.Vector4ToQuaternion(lastRotation)), oldPos);
                        newPos = new Vector4(oldPos.x * transform.x, oldPos.y * transform.y, oldPos.z * transform.z, membraneId);
                        if (lastRotation != Vector4.zero) newPos = MyUtility.QuaternionTransform(MyUtility.Vector4ToQuaternion(lastRotation), newPos);
                        break;
                    case HandleSelectionState.Translate:
                        newPos = new Vector4(oldPos.x + transform.x, oldPos.y + transform.y, oldPos.z + transform.z, membraneId);
                        break;
                    case HandleSelectionState.Rotate:
                        newPos = MyUtility.QuaternionTransform(Quaternion.Inverse(MyUtility.Vector4ToQuaternion(lastRotation)), oldPos);
                        newPos = MyUtility.QuaternionTransform(MyUtility.Vector4ToQuaternion(transform), newPos);
                        break;
                    default:
                        continue;
                }

                newPos /= GlobalProperties.Get.Scale;
                newPos.w = membraneId;
                CPUBuffers.Get.ProteinInstancePositions[i] = newPos;
            }
        }

        CPUBuffers.Get.CopyDataToGPU();

        AdaptCompartmentToTransform(transform, state);
    }

    /**
     * Checks the signed distance for a given compartment object if the extrema of this compartment (bounding box stores the maximal and minimal x-, y-, and z- values).
     * This method checks whether all points lie inside, the result can be simply inverted for outside, can be called with any compartment object (parent or children).
     */
    private bool CheckBoundsForCompartment(CompartmentObject obj)
    {
        return (obj.GetSignedDistance(boundingBox.maxX) <= 0 && //
                obj.GetSignedDistance(boundingBox.maxY) <= 0 && //
                obj.GetSignedDistance(boundingBox.maxZ) <= 0 && //
                obj.GetSignedDistance(boundingBox.minX) <= 0 && //
                obj.GetSignedDistance(boundingBox.minY) <= 0 && //
                obj.GetSignedDistance(boundingBox.minZ) <= 0);
    }

    /**
     * Cleans all data of the current comparment object before destroying it. Values as i.e. the membrane ingredients are not directly bound to
     * the game object and have to be removed before destroying the game object.
     */
    public void CleanBeforeDestroy()
    {
        DepopulateBy(membraneAmount);
        SceneCreatorUIController.Get.lockedIngredientIds.Remove(membraneId);
        membraneId = -1;
        parent = null;
        children.Clear();
        membraneTemplate = null;
        children = null;
        handle = null;
    }

    /**
     * Method must be implemented in subclasses, should be used to update all class variables
     * according to the transform (i.e. for the ellipoid the radii have to be scaled)
     */
    protected abstract void AdaptCompartmentToTransform(Vector4 transform, HandleSelectionState state);

    /**
     * Returns the compartmens unique name, the name is used to distinguish them later on.
     */
    public string GetName()
    {
        return name;
    }

    /**
     * Helper method, which can be used to get the name and, if desired, increment the unique ID of all comparments. Should be removed, error potential.
     */
    protected abstract string GetName(bool increment);

    /**
     * This method was especially introduced for the torus, where a child should not be in the center (there is a hole). For ellipsoid or capsule not 
     * really needed, but for further compartments it should be used to return a valid child position to prevent scenarios, where the collision detection prevents
     * the movement.
     */
    protected abstract Vector3 GetPossibleChildOrigin();

    /**
     * Core method of the compartment object. Every compartment object has to implement a correct signed distance function, which is used in several places of
     * the tool. The signed distance has also to take care of the rotation of the compartment.
     */
    public abstract float GetSignedDistance(Vector3 point);

    /**
     * By passing a ingredient id the membrane is set, the initial amount is @membraneAmount and can later on be de- and increased. The boolean
     * boundingbox initialized is a helper, to prevent that the algorithm uses a wrong bounding box to place the proteins. If in the first call the bounding
     * box is set to i.e. min = 1, 1, 1 and max= 1, 1, 1 all proteins will be sampled at this certain point.
     */
    public void SetMembrane(int ingredientId)
    {
        membraneId = ingredientId;

        if (parent != null)
        {
            HalfSize();
            center = parent.GetPossibleChildOrigin();
        }
        for (int i = 0; i < membraneAmount; i++)
        {
            Vector4 position = DistributeOnAnySurface();

            UpdateBoundingBox(position * GlobalProperties.Get.Scale);

            position.w = membraneId;

            CPUBuffers.Get.ProteinInstancePositions.Add(position);
            CPUBuffers.Get.ProteinInstanceRotations.Add(Vector4.zero);
            CPUBuffers.Get.ProteinInstanceInfos.Add(new Vector4(membraneId, (int)InstanceState.Normal, 0));
        }
        boundingBoxInitialized = true;
        CPUBuffers.Get.CopyDataToGPU();
    }

    /**
     * This method is yet only experimental, templates can be used to use clusters of proteins instead of a single one, which may form
     * certain patterns. These patterns are then used on the membrane, with that a user can model a certain structure as membrane without
     * having to learn it explicitly. Some more functionality would be needed in order to allow rotation or scale without destroying them.
     */
    public void SetMembrane(List<Protein> template)
    {
        membraneTemplate = template;
        membraneId = (int)template[0].Position.w;

        if (parent != null)
        {
            HalfSize();
            center = parent.GetPossibleChildOrigin();
        }

        membraneAmount = membraneAmount - (membraneAmount % template.Count);

        for (int i = 0; i < membraneAmount; i++)
        {
            Vector4 position = DistributeOnAnySurface();
            Vector3 pos = position;

            Vector4 scaledPos = position * GlobalProperties.Get.Scale;
            Quaternion rotation = Quaternion.identity;
            float minDist = float.MaxValue;
            float actDist = 0;
            for (int j = 0; j <= 180; j += 10)
            {
                for (int k = 0; k <= 180; k += 10)
                {
                    for (int l = 0; l <= 180; l += 10)
                    {
                        actDist = 0;
                        foreach (Protein p in membraneTemplate)
                        {
                            Vector4 rotPos = (Quaternion.Euler(j, k, l) * p.Position);
                            actDist += Math.Abs(GetSignedDistance(scaledPos + rotPos * GlobalProperties.Get.Scale));
                        }
                        if (actDist < minDist)
                        {
                            minDist = actDist;
                            rotation = Quaternion.Euler(j, k, l);
                        }
                        else
                        {
                            actDist = 0;
                            foreach (Protein p in membraneTemplate)
                            {
                                Vector4 rotPos = (Quaternion.Euler(j, k, l - 20) * p.Position);
                                actDist += Math.Abs(GetSignedDistance(scaledPos + rotPos * GlobalProperties.Get.Scale));
                            }
                            if (actDist > minDist) break;
                        }
                    }
                }
            }
            foreach (Protein p in membraneTemplate)
            {
                Vector4 templatePos = position + p.Position;

                UpdateBoundingBox(templatePos * GlobalProperties.Get.Scale);
                templatePos.w = (int)membraneTemplate[0].Position.w;

                Vector3 transformVector = pos - center;

                Vector3 transformedPosition = new Vector3(templatePos.x, templatePos.y, templatePos.z) - transformVector;

                Vector3 rotatedPos = rotation * transformedPosition;

                Vector3 backtransformedPos = rotatedPos + transformVector;
                Vector4 endPos = new Vector4(backtransformedPos.x, backtransformedPos.y, backtransformedPos.z, membraneId);


                RotationProbabilityPair pair = new RotationProbabilityPair(MyUtility.Vector4ToQuaternion(p.Rotation), p.ReferenceOrientationAxis, p.ReferenceXAxis, p.ReferenceYAxis, this, false);
                Vector4 endRot = CalculationUtility.GetNewRotationRelativeToCenter(endPos, pair);

                CPUBuffers.Get.ProteinInstancePositions.Add(endPos);
                CPUBuffers.Get.ProteinInstanceRotations.Add(endRot);
                CPUBuffers.Get.ProteinInstanceInfos.Add(p.Information);
            }
        }
        boundingBoxInitialized = true;
        CPUBuffers.Get.CopyDataToGPU();
    }

    /**
     * Updates the bounding box by this position, if the x-, y-, or z- values are larger/smaller than the corresponding min/max vales, they are updated.    
     */
    protected void UpdateBoundingBox(Vector3 position)
    {
        if (position.x > boundingBox.maxX.x) boundingBox.maxX = position;
        if (position.y > boundingBox.maxY.y) boundingBox.maxY = position;
        if (position.z > boundingBox.maxZ.z) boundingBox.maxZ = position;
        if (position.x < boundingBox.minX.x) boundingBox.minX = position;
        if (position.y < boundingBox.minY.y) boundingBox.minY = position;
        if (position.z < boundingBox.minZ.z) boundingBox.minZ = position;
    }

    /**
     * This method is used to modify the current bounding box to check for collisions. The initial bounding box is often copied before modifying 
     * it to check for collisions and revert the modifications if collisions occur.
     */
    protected void ModifyBoundingBox(HandleSelectionState state, Vector3 trans)
    {
        if (state == HandleSelectionState.Scale)
        {
            boundingBox.maxX = new Vector3(boundingBox.maxX.x * trans.x, boundingBox.maxX.y * trans.y, boundingBox.maxX.z * trans.z);
            boundingBox.maxY = new Vector3(boundingBox.maxY.x * trans.x, boundingBox.maxY.y * trans.y, boundingBox.maxY.z * trans.z);
            boundingBox.maxZ = new Vector3(boundingBox.maxZ.x * trans.x, boundingBox.maxZ.y * trans.y, boundingBox.maxZ.z * trans.z);
            boundingBox.minX = new Vector3(boundingBox.minX.x * trans.x, boundingBox.minX.y * trans.y, boundingBox.minX.z * trans.z);
            boundingBox.minY = new Vector3(boundingBox.minY.x * trans.x, boundingBox.minY.y * trans.y, boundingBox.minY.z * trans.z);
            boundingBox.minZ = new Vector3(boundingBox.minZ.x * trans.x, boundingBox.minZ.y * trans.y, boundingBox.minZ.z * trans.z);
        }
        else if (state == HandleSelectionState.Translate)
        {
            boundingBox.maxX += trans;
            boundingBox.maxY += trans;
            boundingBox.maxZ += trans;
            boundingBox.minX += trans;
            boundingBox.minY += trans;
            boundingBox.minZ += trans;
        }
        else if (state == HandleSelectionState.Rotate)
        {
            //Has to be recalculated            
            boundingBox.maxX = Vector3.zero;
            boundingBox.maxY = Vector3.zero;
            boundingBox.maxZ = Vector3.zero;
            boundingBox.minX = Vector3.zero;
            boundingBox.minY = Vector3.zero;
            boundingBox.minZ = Vector3.zero;

            foreach (Vector4 position in CPUBuffers.Get.ProteinInstancePositions) if ((int)position.w == membraneId) UpdateBoundingBox(position * GlobalProperties.Get.Scale);
        }
    }

    /**
     * Removes a given child of the current compartment object. 
     */
    public void RemoveChild(CompartmentObject compartmentToRemove)
    {
        this.children.Remove(compartmentToRemove);
    }

    /**
     * Setter for the unique name. The name should be set by the user at some point, to give the compartments meaningful names. These compartment names could also
     * be used for the treeview in the UI (yet only in the initial HI-Virus) to show cuts and volumes of interest to the user.
     */
    public void SetName()
    {
        string finalName = "";
        CompartmentObject p = parent;
        while (p != null)
        {
            finalName += SUB;
            p = p.parent;
        }
        finalName += GetName(true);
        name = finalName;
    }

    /**
     * The initial dimensions of a compartment object are static, if the user adds children they should be smaller to prevent collisions or
     * confusing scenarios. The half size method basically takes the half of the initial dimensions.
     */
    public abstract void HalfSize();

    /**
     * Returns the min x- y- and z- values
     */
    public Vector3 GetBoundsMin()
    {
        return new Vector3(boundingBox.minX.x, boundingBox.minY.y, boundingBox.minZ.z);
    }

    /**
     * Returns the min x- y- and z- values
     */
    public Vector3 GetBoundsMax()
    {
        return new Vector3(boundingBox.maxX.x, boundingBox.maxY.y, boundingBox.maxZ.z);
    }

    /**
     * Returns a list of all children of this compartment if any
     */
    public List<CompartmentObject> GetChildren()
    {
        return children;
    }

    /**
     * Adds a child to this compartment
     */
    public void AddChild(CompartmentObject comp)
    {
        children.Add(comp);
    }

    /**
     * Returns the parent compartment if any
     */
    public CompartmentObject GetParent()
    {
        return parent;
    }

    /**
     * Sets the enclosing compartment, an object can only define one enclosing compartment
     */
    public void AddParent(CompartmentObject parent)
    {
        this.parent = parent;
    }

    /**
     * Returns the center point of the current compartment
     */
    public Vector3 GetCenter()
    {
        return center;
    }

    /**
     * Setter for the center of the compartment. The center is used for the signed distance function and is also the origin of 
     * the transform handle, if later the compartments can be rotated around an arbitrarily point this method should be used to adjust the center.
     */
    public void SetCenter(Vector3 center)
    {
        this.center = center;
    }

    /**
     * Shows or hides the handle for the current compartment. It should be prevented to show multiple handles (multiple compartments, 
     * the protein handle, cut object handle... to prevent confusion and malfunction).
     */
    public void ShowHandle(bool show, HandleSelectionState state)
    {
        handle.enabled = show;
        if (show)
        {
            handle.Enable();
            handle.gameObject.SetActive(show);
            handle.transform.localPosition = center;
            handle.SetSelectionState(state);
            handle.gameObject.SetActive(true);
        }
        else
        {
            handle.Disable();
            handle.gameObject.SetActive(false);
        }
    }

    /**
     * Returns the current handle selection state (translate, scale, rotate)
     */
    public HandleSelectionState GetHandleSelectionState()
    {
        return handle.GetSelectionState();
    }

    /**
     * Method returns a position on the surface of this compartment, randomly samples a point within the bounding box of this compartment.
     * This point is then passed to the calculation utility which returns the nearest surface point to the given random sample point.
     * Returns the scaled position with the correct w-component
     */
    protected Vector4 DistributeOnAnySurface()
    {
        Vector3 min;
        Vector3 max;
        if (!boundingBoxInitialized)
        {
            min = new Vector3(-50.0f, -50.0f, -50.0f);
            max = new Vector3(50.0f, 50.0f, 50.0f);
        }
        else
        {
            min = new Vector3(boundingBox.minX.x, boundingBox.minY.y, boundingBox.minZ.z);
            max = new Vector3(boundingBox.maxX.x, boundingBox.maxY.y, boundingBox.maxZ.z);
        }
        Vector4 newPos = CalculationUtility.GetNearestSurfacePoint(this, new Vector3(UnityEngine.Random.Range(min.x, max.x), UnityEngine.Random.Range(min.y, max.y), UnityEngine.Random.Range(min.z, max.z)), Vector3.zero) / GlobalProperties.Get.Scale;
        return newPos;
    }

    /**
     * Returns the amount of membrane ingredients of this compartment
     */
    public int GetMembraneAmount()
    {
        return membraneAmount;
    }

    /**
     * Increases the amount of membrane ingredients of this compartment
     */
    public void PopulateBy(int amount)
    {
        //if template is used, stepsize has to be adapted (n-proteins per template)
        if (amount < 0) amount *= -1;
        membraneAmount += amount;

        if (membraneTemplate != null) amount /= membraneTemplate.Count;

        UnityEngine.Debug.Log("P: " + amount + " S: " + membraneAmount);
        for (int i = 0; i < amount; i++)
        {
            if (membraneTemplate != null)
            {
                Vector4 position = DistributeOnAnySurface();
                Vector3 pos = position;

                Vector4 scaledPos = position * GlobalProperties.Get.Scale;
                Quaternion rotation = Quaternion.identity;
                float minDist = float.MaxValue;
                float actDist = 0;
                for (int j = 0; j <= 180; j += 10)
                {
                    for (int k = 0; k <= 180; k += 10)
                    {
                        for (int l = 0; l <= 180; l += 10)
                        {
                            actDist = 0;
                            foreach (Protein p in membraneTemplate)
                            {
                                Vector4 rotPos = (Quaternion.Euler(j, k, l) * p.Position);
                                actDist += Math.Abs(GetSignedDistance(scaledPos + rotPos * GlobalProperties.Get.Scale));
                            }
                            if (actDist < minDist)
                            {
                                minDist = actDist;
                                rotation = Quaternion.Euler(j, k, l);
                            }
                            else
                            {
                                actDist = 0;
                                foreach (Protein p in membraneTemplate)
                                {
                                    Vector4 rotPos = (Quaternion.Euler(j, k, l - 20) * p.Position);
                                    actDist += Math.Abs(GetSignedDistance(scaledPos + rotPos * GlobalProperties.Get.Scale));
                                }
                                if (actDist > minDist) break;
                            }
                        }
                    }
                }
                foreach (Protein p in membraneTemplate)
                {
                    Vector4 templatePos = position + p.Position;

                    UpdateBoundingBox(templatePos * GlobalProperties.Get.Scale);
                    templatePos.w = (int)membraneTemplate[0].Position.w;

                    Vector3 transformVector = pos - center;

                    Vector3 transformedPosition = new Vector3(templatePos.x, templatePos.y, templatePos.z) - transformVector;

                    Vector3 rotatedPos = rotation * transformedPosition;

                    Vector3 backtransformedPos = rotatedPos + transformVector;
                    Vector4 endPos = new Vector4(backtransformedPos.x, backtransformedPos.y, backtransformedPos.z, membraneId);


                    RotationProbabilityPair pair = new RotationProbabilityPair(MyUtility.Vector4ToQuaternion(p.Rotation), p.ReferenceOrientationAxis, p.ReferenceXAxis, p.ReferenceYAxis, this, false);
                    Vector4 endRot = CalculationUtility.GetNewRotationRelativeToCenter(endPos, pair);

                    CPUBuffers.Get.ProteinInstancePositions.Add(endPos);
                    CPUBuffers.Get.ProteinInstanceRotations.Add(endRot);
                    CPUBuffers.Get.ProteinInstanceInfos.Add(p.Information);
                }
            }
            else
            {
                Vector4 position = DistributeOnAnySurface();
                position.w = membraneId;
                CPUBuffers.Get.ProteinInstancePositions.Add(position);
                CPUBuffers.Get.ProteinInstanceRotations.Add(Vector4.zero);
                CPUBuffers.Get.ProteinInstanceInfos.Add(new Vector4(membraneId, (int)InstanceState.Normal, 0));
            }
        }
        CPUBuffers.Get.CopyDataToGPU();
    }

    /**
     * Decreases the amount of membrane ingredients of this compartment
     */
    public void DepopulateBy(int amount)
    {
        if (amount > 0) amount *= -1;

        membraneAmount += amount;

        List<Vector4> positions = new List<Vector4>();
        List<Vector4> rotations = new List<Vector4>();
        List<Vector4> infos = new List<Vector4>();

        int count = 0;
        for (int i = 0; i < CPUBuffers.Get.ProteinInstancePositions.Count; i++)
        {
            if (CPUBuffers.Get.ProteinInstancePositions[i].w == membraneId)
            {
                count++;
                if (count <= membraneAmount)
                {
                    positions.Add(CPUBuffers.Get.ProteinInstancePositions[i]);
                    rotations.Add(CPUBuffers.Get.ProteinInstanceRotations[i]);
                    infos.Add(CPUBuffers.Get.ProteinInstanceInfos[i]);
                }
            }
            else
            {
                positions.Add(CPUBuffers.Get.ProteinInstancePositions[i]);
                rotations.Add(CPUBuffers.Get.ProteinInstanceRotations[i]);
                infos.Add(CPUBuffers.Get.ProteinInstanceInfos[i]);
            }
        }
        CPUBuffers.Get.ProteinInstancePositions.Clear();
        CPUBuffers.Get.ProteinInstanceRotations.Clear();
        CPUBuffers.Get.ProteinInstanceInfos.Clear();

        CPUBuffers.Get.ProteinInstancePositions = new List<Vector4>(positions);
        CPUBuffers.Get.ProteinInstanceRotations = new List<Vector4>(rotations);
        CPUBuffers.Get.ProteinInstanceInfos = new List<Vector4>(infos);
        positions.Clear();
        rotations.Clear();
        infos.Clear();
        CPUBuffers.Get.CopyDataToGPU();
    }

    /**
     * Hides the (scale, rotate, move) handle of this compartment object
     */
    public void HideHandle()
    {
        ShowHandle(false, HandleSelectionState.Translate);
    }
}