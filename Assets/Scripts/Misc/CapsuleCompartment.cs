﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Assets.Scripts.Loaders;
using Assets.Scripts.Misc;


public class CapsuleCompartment : CompartmentObject
{
    protected string NAME = "Capsule";
    private static int ID = 0;

    public Vector3 capsuleA = new Vector3(0.0f, 20.0f, 0.0f);
    public Vector3 capsuleB = new Vector3(0.0f, -20.0f, 0.0f);
    public Vector3 capsuleRadius = new Vector3(20.0f, 20.0f, 20.0f);

    public CapsuleCompartment() { }

    // Initialization
    void Start()
    {
        lastScale = transform.localScale;
        compartmentType = CompartmentType.Capsule;
    }

    /**
      * Adapts all member variables to the transform, defined by the type @state and a vector
      * containing the change.
      */
    protected override void AdaptCompartmentToTransform(Vector4 transform, HandleSelectionState state)
    {       
        switch (state)
        {
            case HandleSelectionState.Scale:
                lastScale = handle.transform.localScale;
                capsuleRadius = new Vector3(capsuleRadius.x * transform.x, capsuleRadius.y * transform.y, capsuleRadius.z * transform.z);
                capsuleA = new Vector3(capsuleA.x * transform.x, capsuleA.y * transform.y, capsuleA.z * transform.z);
                capsuleB = new Vector3(capsuleB.x * transform.x, capsuleB.y * transform.y, capsuleB.z * transform.z);
                break;
            case HandleSelectionState.Translate:
                lastPosition = handle.transform.localPosition;
                center = new Vector3(center.x + transform.x, center.y + transform.y, center.z + transform.z);
                capsuleA = new Vector3(capsuleA.x + transform.x, capsuleA.y + transform.y, capsuleA.z + transform.z);
                capsuleB = new Vector3(capsuleB.x + transform.x, capsuleB.y + transform.y, capsuleB.z + transform.z);
                break;
            case HandleSelectionState.Rotate:
                capsuleA = MyUtility.QuaternionTransform(Quaternion.Inverse(MyUtility.Vector4ToQuaternion(lastRotation)), capsuleA);
                capsuleA = MyUtility.QuaternionTransform(MyUtility.Vector4ToQuaternion(transform), capsuleA);
                capsuleB = MyUtility.QuaternionTransform(Quaternion.Inverse(MyUtility.Vector4ToQuaternion(lastRotation)), capsuleB);
                capsuleB = MyUtility.QuaternionTransform(MyUtility.Vector4ToQuaternion(transform), capsuleB);
                center = MyUtility.QuaternionTransform(Quaternion.Inverse(MyUtility.Vector4ToQuaternion(lastRotation)), center);
                center = MyUtility.QuaternionTransform(MyUtility.Vector4ToQuaternion(transform), center);
                lastRotation = transform;
                break;
        }
    }

    /**
     * Returns the signed distance for the point, considers the current rotation, other transforms are considered through the previous
     * modification of the member variables (i.e. radii).
     */
    public override float GetSignedDistance(Vector3 point)
    {
        Vector3 cA = MyUtility.QuaternionTransform(Quaternion.Inverse(MyUtility.Vector4ToQuaternion(lastRotation)), capsuleA);
        Vector3 cB = MyUtility.QuaternionTransform(Quaternion.Inverse(MyUtility.Vector4ToQuaternion(lastRotation)), capsuleB);
        Vector3 p = MyUtility.QuaternionTransform(Quaternion.Inverse(MyUtility.Vector4ToQuaternion(lastRotation)), point);

        Vector3 pa = p - cA, ba = cB - cA;
        float h = Mathf.Clamp(Vector3.Dot(pa, ba) / Vector3.Dot(ba, ba), 0.0f, 1.0f);
        Vector3 v = pa - ba * h;
        return (float)(Vector3.Scale(v, new Vector3(1 / capsuleRadius.x, 1 / capsuleRadius.y, 1 / capsuleRadius.z)).magnitude - 1.0) * Mathf.Min(Mathf.Min(capsuleRadius.x, capsuleRadius.y), capsuleRadius.z);
    }       

    /**
     * Halfs the size of the current compartment object, used for children, which should be smaller than their parent to prevent collision issues
     */
    public override void HalfSize()
    {
        capsuleA.y /= 2.0f;
        capsuleB.y /= 2.0f;
        capsuleRadius = new Vector3(capsuleRadius.x / 2.0f, capsuleRadius.y / 2.0f, capsuleRadius.z / 2.0f);
    }

    /**
     * Returns the name of the current compartment object. A bool can be passed to increment the static and unique compartment ID.
     */
    protected override string GetName(bool increment)
    {
        if (increment) ID++;
        return NAME + ID;
    }

    /**
     * Returns a possible child origin. For capsule the center is the best choice.
     */
    protected override Vector3 GetPossibleChildOrigin()
    {
        return center;
    }
}