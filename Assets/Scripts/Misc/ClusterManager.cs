﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Assets.Scripts.Misc
{
    /**
     * Cluster Manager is used to store and create/destroy protein clusters. The treenodes hold the information about the clusterID they belong to.
     * This is needed because if different ingredients form a cluster, each of them has to hold the information about the cluster they are part of.
     */
    [JsonObject(MemberSerialization.OptIn)]
    public static class ClusterManager
    {
        [JsonProperty]
        public static Dictionary<int, ProteinCluster> clusters = new Dictionary<int, ProteinCluster>();

        /**
         * Adds the given cluster to the clusterlist
         */
        private static int AddCluster(ProteinCluster cluster)
        {
            int id = clusters.Count;
            clusters.Add(id, cluster);
            return id;
        }

        /**
         * Removes the cluster, identified by the id. This method is called, when the user reduces the clustered protein amount when only two are left,
         * leading to a resolution of this cluster.
         */
        public static void RemoveCluster(int id)
        {
            if (clusters.ContainsKey(id)) clusters.Remove(id);
        }

        /**
         * Returns a cluster for the given id, the tree nodes hold only this information, to keep it synchronized over multiple tree nodes
         */
        public static ProteinCluster GetClusterForId(int id)
        {
            if (clusters.ContainsKey(id)) return clusters[id];
            return null;
        }

        /**
         * Forms or increases a cluster. if no cluster exists, both ids are added, so ingredients form clusters with different proteins.
         * 
         * @param clusterId is the id of the possibly already existent cluster, if not a new one is formed and returned
         * @param ingredientId1 is the id of the moved ingredient
         * @param ingredientId2 is the id of the approached ingredient. This is only used, if no cluster exists, otherwise the new id joins the cluster.
         * @param compartment the compartment for the cluster
         * @param distance the distance between the ingredients
         * 
         * @return the assigned cluster id
         */
        public static int FormCluster(int clusterId, int ingredientId1, int ingredientId2, CompartmentObject compartment, float distance)
        {
            if (clusters.ContainsKey(clusterId))
            {
                if (clusters[clusterId] != null && clusters[clusterId].IsWithinThreshold(distance))
                {
                    //Increase cluster size
                    clusters[clusterId].IncreaseClusterSize(ingredientId1);
                    clusters[clusterId].AdjustDistance(distance);
                }
                else if (clusters[clusterId] != null)
                {
                    //Reduce cluster size
                    bool shouldBeDeleted = clusters[clusterId].DecreaseClusterSize(ingredientId1);
                    if (shouldBeDeleted)
                    {
                        clusters[clusterId] = null;
                        RemoveCluster(clusterId);
                        return -1;
                    }
                }
            }
            else
            {
                //Create cluster
                clusterId = AddCluster(new ProteinCluster(compartment, distance, ingredientId1, ingredientId2));
            }
            return clusterId;
        }
    }
}
