﻿using System;
using System.Collections.Generic;
using System.Linq;
using UIWidgets;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Utils;
using Assets.Scripts.Misc;
using Assets.Scripts.Loaders;

public class SceneCreatorUIController : MonoBehaviour
{
    private static SceneCreatorUIController _instance = null;                                                           //singleton scene creator instance
    private static readonly string useTemplate = "Use template..";                                                      //static string for the usage of templates

    public Toggle showInfo;                                                                                             //toggle to select whether to show or hide ingredient descriptions
    public Toggle detectCollisions;                                                                                     //toggle to select whether to check for collisions or not

    private Dictionary<int, int> collisionHelper = new Dictionary<int, int>();                                          //collision helper, stores the amount of collisions of the same ingredient for break conditions
    private Dictionary<int, CompartmentObject> collisionCompartmentHelper = new Dictionary<int, CompartmentObject>();   //stores the corresponding compartment object for collisions, otherwise ingredients are not bound to it anymore

    public static bool createTemplate = false;                                                                          //whether it is template creation mode or not
    public static bool compartmentMode = true;                                                                          //whether it is compartment creation mode or not

    //Class variables
    private int currentlySelectedIngredientIndex = -1;                                                                  //index of the currently selected ingredient
    private int currentMaxIngredientId = 0;                                                                             //stores the max id, used when loading ingredients or downloading new ones

    private Vector4 startPosition = Vector4.zero;                                                                       //stores the startposition of a ingredient when the user starts moving it
    private Vector4 startRotation = Vector4.zero;                                                                       //stores the startrotation of a ingredient when the user starts rotating it

    public List<int> lockedIngredientIds = new List<int>();                                                             //locks compartment ingredients, do not allow moving them to preserve the structure

    public Dictionary<int, Bounds> proteinBoundingBoxes = new Dictionary<int, Bounds>();                                //UNUSED, stores the bounding boxes of the ingredients
    public Dictionary<String, Ingredient> AvailableIngredients = new Dictionary<String, Ingredient>();                  //stores all available ingredients

    private Dictionary<int, MovingProtein> MovingProteins = new Dictionary<int, MovingProtein>();                       //holds all moving proteins, their positions are updated each frame, when destination is reached they are removed

    private CustomStack<CustomPair<Action, Action>> Backstack = new CustomStack<CustomPair<Action, Action>>();          //Backstack holds undo actions and the corresponding redo actions
    private CustomStack<CustomPair<Action, Action>> Frontstack = new CustomStack<CustomPair<Action, Action>>();         //Frontstack holds redo actions and the corresponding undo actions

    public TransformHandle transformHandle;                                                                             //the (GameObject) transform handle, used for ingredient movement/rotation
    private HandleSelectionState currentSelectionState = HandleSelectionState.Translate;                                //the current selection state of the handle

    //UI for Compartments
    public ListView CompartmentsListView;                                                                               //ListView containing all compartments
    public InputField downloadPDBNameComp;                                                                              //InputField for the name of the -to-download- pdb file (compartment panel)
    public InputField membraneMaxAmount;                                                                                //InputField for the maximum amount of membrane proteins, individually setable
    public Dropdown compTypeDropdown;                                                                                   //Dropdown of the compartment type (shape)
    public Dropdown membraneDropdown;                                                                                   //Dropdown of the membrane ingredient, which should be used to create the compartment
    public Slider membraneAmountSlider;                                                                                 //Slider for the amount of ingredients in the membrane

    //UI for Ingredients
    public Dropdown ingredientDropdown;                                                                                 //Dropdown for the ingredient that can be added to the scene
    public InputField ingredientAmountField;                                                                            //InputField for the amount of ingredients that should be added
    public InputField downloadPDBNameIng;                                                                               //InputField for the name of the -to-download- pdb file (ingredient panel)
    public InputField membraneVariance;                                                                                 //InputField for the variance of the membrane ingredient rotations
    public InputField ingredientVariance;                                                                               //InputField for the variance of the ingredient rotations (ingredient panel)

    private int lastSelectedCompartmentIndex = -1;

    /**
     * Returns the singleton instance of the scene creator
     */
    public static SceneCreatorUIController Get
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<SceneCreatorUIController>();
                if (_instance == null)
                {
                    var go = GameObject.Find("_SceneCreatorUIController");
                    if (go != null)
                        DestroyImmediate(go);

                    go = new GameObject("_SceneCreatorUIController") { hideFlags = HideFlags.HideInInspector };
                    _instance = go.AddComponent<SceneCreatorUIController>();
                }
            }
            return _instance;
        }
    }

    /**
     * Initialization, adds a transform handle for the ingredients and resets the camera.
     */
    void Start()
    {
        transformHandle = gameObject.GetComponent<TransformHandle>();
        transformHandle.Disable();
        GameObject.Find("Reset Camera Button").GetComponent<Button>().onClick.Invoke();
    }

    /**
     * Resets all necessary components of the scene creator and centers the camera
     */
    public void Reset()
    {
        lockedIngredientIds.Clear();
        currentlySelectedIngredientIndex = -1;
        startPosition = Vector4.zero;
        startRotation = Vector4.zero;
        createTemplate = false;

        Backstack.Clear();
        Frontstack.Clear();

        CompartmentObjectManager.Get.RemoveAllCompartments();
        DecisionTreeManager.Get.Reset();

        GameObject.Find("Reset Camera Button").GetComponent<Button>().onClick.Invoke();
    }

    /**
     * Method called to redo the latest actions, adds the undone action-pair to the backstack for undo actions.
     */
    public void Redo()
    {
        transformHandle.Disable();
        if (Frontstack.Count > 0)
        {
            CustomPair<Action, Action> a = Frontstack.Pop();
            AddToBackstack(a, false);
            a.GetForwardAction().Invoke();
        }
    }

    /**
     * Method called to undo the latest actions, adds the undone action-pair to the frontstack for redo actions.
     */
    public void Undo()
    {
        transformHandle.Disable();
        if (Backstack.Count > 0)
        {
            CustomPair<Action, Action> a = Backstack.Pop();
            AddToFrontstack(a);
            a.GetBackwardAction().Invoke();
        }
    }

    /**
     * Adds an action-pair to the backstack, for undo and redo actions. If newAction is set to true the frontstack gets cleared.
     */
    private void AddToBackstack(CustomPair<Action, Action> action, bool newAction)
    {
        Backstack = Backstack.PushMaintainingSize(action);
        if (newAction) Frontstack.Clear();
    }

    /**
     * Adds an action-pair to the frontstack for redo and undo actions
     */
    private void AddToFrontstack(CustomPair<Action, Action> action)
    {
        Frontstack = Frontstack.PushMaintainingSize(action);
    }

    /**
     * Update once per frame, the below documented steps depend on certain conditions, only necessary stuff is updated.
     */
    public void Update()
    {
        CheckFuzzy();

        //Detect collisions
        if (detectCollisions.isOn)
        {
            CheckCollisions();
        }
        else
        {
            collisionHelper.Clear();
        }

        //Update position and rotation of selected
        if (transformHandle.enabled && currentlySelectedIngredientIndex >= 0)
        {
            UpdateSelected();
        }

        //Update moving proteins
        if (MovingProteins.Count > 0)
        {
            UpdateMovingProteins();
        }

        if (CompartmentsListView.SelectedIndex == -1 && CompartmentsListView.DataSource.Count >= 1)
        {
            CompartmentsListView.SelectedIndex = 0;
        }

        if (CompartmentsListView.SelectedIndex >= CompartmentsListView.DataSource.Count)
        {
            CompartmentsListView.SelectedIndex = CompartmentsListView.DataSource.Count - 1;
            CompartmentObjectManager.Get.SelectedCompartmentObject = CompartmentsListView.SelectedIndex;
        }
    }

    /**
     * Checks for collisions, if any they are resolved. After a certain amount of checks without a fitting position some ingredients are discarded.
     */
    public void CheckCollisions()
    {
        int[] collisions = new int[GPUBuffers.Get.ProteinInstanceOverlapping.count];
        GPUBuffers.Get.ProteinInstanceOverlapping.GetData(collisions);

        int movedOnes = 0;
        List<int> toRemove = new List<int>();
        for (int i = 0; i < collisions.Length; i++)
        {
            if (i >= CPUBuffers.Get.ProteinInstancePositions.Count) break;

            int id = (int)CPUBuffers.Get.ProteinInstancePositions[i].w;

            if (!collisionHelper.ContainsKey(i)) collisionHelper.Add(i, 0);
            if (collisionCompartmentHelper.ContainsKey(id) && collisionCompartmentHelper[id] == null) collisionCompartmentHelper.Remove(id);
            if (!collisionCompartmentHelper.ContainsKey(id))
            {
                List<TreeNode> tree = DecisionTreeManager.Get.GetAllNodes(id);
                CompartmentObject compartment = null;
                foreach (TreeNode node in tree)
                {
                    if (node.GetCompartmentObject() == null) continue;

                    //On membrane
                    if (node.GetCompartmentObject().membraneId == id)
                    {
                        compartment = node.GetCompartmentObject();
                        break;
                    }
                    //Inside
                    else if (node.GetCompartmentObject().GetSignedDistance(CPUBuffers.Get.ProteinInstancePositions[i] * GlobalProperties.Get.Scale) < 0.0f)
                    {
                        compartment = node.GetCompartmentObject();
                    }
                    //Outside
                    else
                    {
                        compartment = node.GetCompartmentObject();
                        break;
                    }
                }
                collisionCompartmentHelper.Add(id, compartment);
            }
            if (collisionHelper[i] > 500)
            {
                toRemove.Add(i);
            }
            else if (collisions[i] > 0 && i < CPUBuffers.Get.ProteinInstancePositions.Count && collisionHelper[i] >= 0)
            {
                movedOnes++;
                collisionHelper[i] = collisionHelper[i] + 1;
                ResolveCollision(i);
            }
            else if (collisions[i] == 0)
            {
                Vector4 pos = CPUBuffers.Get.ProteinInstancePositions[i];
                CompartmentObject cO = collisionCompartmentHelper[(int)pos.w];
                if (cO.membraneId == (int)pos.w || cO.GetSignedDistance(pos * GlobalProperties.Get.Scale) > 2.0f || cO.GetSignedDistance(pos * GlobalProperties.Get.Scale) < -2.0f) collisionHelper[i] = -1;
                else
                {
                    if (cO.GetSignedDistance(pos * GlobalProperties.Get.Scale) > 2.0f) CPUBuffers.Get.ProteinInstancePositions[i] = pos + new Vector4(20, 20, 20, 0);
                    else if (cO.GetSignedDistance(pos * GlobalProperties.Get.Scale) < -2.0f) CPUBuffers.Get.ProteinInstancePositions[i] = pos - new Vector4(20, 20, 20, 0);
                }
            }
            else if (i >= CPUBuffers.Get.ProteinInstancePositions.Count) break;
        }
        if (movedOnes == 0)
        {
            toRemove.Sort();
            toRemove.Reverse();
            foreach (int i in toRemove)
            {
                CPUBuffers.Get.ProteinInstancePositions.RemoveAt(i);
                CPUBuffers.Get.ProteinInstanceRotations.RemoveAt(i);
                CPUBuffers.Get.ProteinInstanceInfos.RemoveAt(i);
                collisionHelper.Clear();
                detectCollisions.isOn = false;
            }
        }
        CPUBuffers.Get.CopyDataToGPU();
    }

    /**
     * Method to resolve collisions in the scene, called only, when the checkbox in the UI is toggled. Resolves the collision by a simple schema,
     * the ingredient moves by a randomly selected vector until no collision happens. After a certain amount of collision tests the ingredient gets
     * discarded, it is assumed, that no free position is left for the colliding ingredient.
     */
    public void ResolveCollision(int index)
    {
        int breakCounter = 0;
        float scale = GlobalProperties.Get.Scale;
        float min = -20.0f * scale;
        float max = 20.0f * scale;

        CompartmentObject compartment = collisionCompartmentHelper[(int)CPUBuffers.Get.ProteinInstancePositions[index].w];
        Vector4 position = CPUBuffers.Get.ProteinInstancePositions[index];
        int id = (int)position.w;
        position *= scale;
        Vector4 newPos = position + new Vector4(UnityEngine.Random.Range(min, max), UnityEngine.Random.Range(min, max), UnityEngine.Random.Range(min, max), 0);

        //On surface, keep it there
        if (compartment.membraneId == id)
        {
            newPos = CalculationUtility.GetNearestSurfacePoint(compartment, newPos, Vector3.zero);
        }
        //Inside, keep it there
        else if (compartment.GetSignedDistance(position) < 0.0f)
        {
            while (compartment.GetSignedDistance(newPos) > -2.0f)
            {
                newPos = position + new Vector4(UnityEngine.Random.Range(min, max), UnityEngine.Random.Range(min, max), UnityEngine.Random.Range(min, max), 0);
                breakCounter++;
                if (breakCounter > 500) break;
            }
        }
        //Outside, keep it there
        else
        {
            while (compartment.GetSignedDistance(newPos) > 2.0f)
            {
                newPos = position + new Vector4(UnityEngine.Random.Range(min, max), UnityEngine.Random.Range(min, max), UnityEngine.Random.Range(min, max), 0);
                breakCounter++;
                if (breakCounter > 500) break;
            }
        }

        //If breakCounter larger no position has been found, so return before writing it to buffer
        if (breakCounter >= 500) return;

        position.x = newPos.x;
        position.y = newPos.y;
        position.z = newPos.z;
        position /= scale;
        position.w = id;
        CPUBuffers.Get.ProteinInstancePositions[index] = position;
    }

    /**
     * Updates the currently selected ingredient position or orientation, based on the current handle selection state and position/orientation.
     */
    public void UpdateSelected()
    {
        if (currentSelectionState == HandleSelectionState.Translate)
        {
            Vector4 newPosition = transformHandle.transform.position / GlobalProperties.Get.Scale;
            newPosition.w = CPUBuffers.Get.ProteinInstancePositions[currentlySelectedIngredientIndex].w;
            if (CPUBuffers.Get.ProteinInstancePositions[currentlySelectedIngredientIndex] != newPosition)
            {
                CPUBuffers.Get.ProteinInstancePositions[currentlySelectedIngredientIndex] = newPosition;
                CPUBuffers.Get.CopyDataToGPU();
            }
        }
        else if (currentSelectionState == HandleSelectionState.Rotate)
        {
            Vector4 newRotation = new Vector4(transformHandle.transform.rotation.x, transformHandle.transform.rotation.y, transformHandle.transform.rotation.z, transformHandle.transform.rotation.w);
            if (CPUBuffers.Get.ProteinInstanceRotations[currentlySelectedIngredientIndex] != newRotation)
            {
                CPUBuffers.Get.ProteinInstanceRotations[currentlySelectedIngredientIndex] = newRotation;
                CPUBuffers.Get.CopyDataToGPU();
            }
        }
    }

    /**
     * Updates all moving proteins (positions and rotations). Is called in update while the moving proteins have not reached their destination.
     */
    public void UpdateMovingProteins()
    {
        List<int> toRemove = new List<int>();
        foreach (int index in MovingProteins.Keys)
        {
            if (!MovingProteins[index].HasReachedDestination())
            {
                if (MovingProteins[index].IsMoving()) CPUBuffers.Get.ProteinInstancePositions[index] = MovingProteins[index].GetInterpolatedPosition();
                if (MovingProteins[index].IsRotating()) CPUBuffers.Get.ProteinInstanceRotations[index] = MovingProteins[index].GetInterpolatedRotation();
            }
            else
            {
                if (MovingProteins[index].IsMoving()) CPUBuffers.Get.ProteinInstancePositions[index] = MovingProteins[index].GetInterpolatedPosition();
                if (MovingProteins[index].IsRotating()) CPUBuffers.Get.ProteinInstanceRotations[index] = MovingProteins[index].GetInterpolatedRotation();
                toRemove.Add(index);
            }
        }
        foreach (int index in toRemove)
            MovingProteins.Remove(index);

        CPUBuffers.Get.CopyDataToGPU();
    }

    /**
     * Method to add an ingredient, checks if the value is valid before calling the method -AddProteinIngredient-. This call could be extended to
     * add other ingredients as well, such as lipids, RNA and so on.
     */
    public void AddIngredient()
    {
        if (!string.IsNullOrEmpty(ingredientDropdown.options.ElementAt(ingredientDropdown.value).text) && AvailableIngredients.ContainsKey(ingredientDropdown.options.ElementAt(ingredientDropdown.value).text))
        {
            AddProteinIngredient();
        }
    }

    /**
     * Method to add a protein ingredient. The position is randomly selected, the rotation is set to zero. The amount is read from 
     * the corresponding UI input field. 
     */
    public void AddProteinIngredient()
    {
        Camera main = Camera.main;
        int numInstances = 0;
        int.TryParse(ingredientAmountField.text, out numInstances);

        Ingredient toAdd = AvailableIngredients[ingredientDropdown.options.ElementAt(ingredientDropdown.value).text];

        List<Vector4> positionsToAdd = new List<Vector4>();
        List<Vector4> rotationsToAdd = new List<Vector4>();
        List<Vector4> infosToAdd = new List<Vector4>();

        float minX = (createTemplate ? 5.0f : -100.0f);
        float minY = (createTemplate ? -10.0f : -100.0f);
        float minZ = (createTemplate ? 0.0f : -100.0f);
        float maxX = (createTemplate ? 25.0f : 100.0f);
        float maxY = (createTemplate ? 10.0f : 100.0f);
        float maxZ = (createTemplate ? 0.0f : 100.0f);

        for (var i = 0; i < numInstances; i++)
        {
            Vector4 position = new Vector4(UnityEngine.Random.Range(minX, maxX), UnityEngine.Random.Range(minY, maxY), UnityEngine.Random.Range(minZ, maxZ));
            position /= GlobalProperties.Get.Scale;
            position.w = toAdd._ingredientId;

            Vector4 rotation = Vector4.zero;

            positionsToAdd.Add(position);
            rotationsToAdd.Add(rotation);
            infosToAdd.Add(new Vector4(toAdd._ingredientId, (int)InstanceState.Normal, 0));
        }

        Action forwardAction = delegate
        {
            SceneManager.Get.ProteinInstanceCount += numInstances;
            for (var i = 0; i < SceneManager.Get.IngredientGroups.Count; i++)
            {
                if (SceneManager.Get.IngredientGroups[i]._ingredients.Contains(toAdd))
                {
                    SceneManager.Get.IngredientGroups[i].NumIngredients += numInstances;
                    break;
                }
            }

            CPUBuffers.Get.ProteinInstancePositions.AddRange(positionsToAdd);
            CPUBuffers.Get.ProteinInstanceRotations.AddRange(rotationsToAdd);
            CPUBuffers.Get.ProteinInstanceInfos.AddRange(infosToAdd);

            CPUBuffers.Get.CopyDataToGPU();
        };

        Action backAction = delegate
        {
            SceneManager.Get.ProteinInstanceCount -= numInstances;
            for (var i = 0; i < SceneManager.Get.IngredientGroups.Count; i++)
            {
                if (SceneManager.Get.IngredientGroups[i]._ingredients.Contains(toAdd))
                {
                    SceneManager.Get.IngredientGroups[i].NumIngredients -= numInstances;
                    break;
                }
            }

            for (int i = 0; i < numInstances; i++)
            {
                CPUBuffers.Get.ProteinInstancePositions.Remove(positionsToAdd[i]);
                CPUBuffers.Get.ProteinInstanceRotations.Remove(rotationsToAdd[i]);
                CPUBuffers.Get.ProteinInstanceInfos.Remove(infosToAdd[i]);
            }
            CPUBuffers.Get.CopyDataToGPU();
        };

        AddToBackstack(new CustomPair<Action, Action>(backAction, forwardAction), true);
        ingredientAmountField.text = "";

        forwardAction.Invoke();
    }

    /**
     * Adds or updates a moving protein. Moving proteins are proteins, which have a given start- and endposition. If a moving protein with the given
     * id already exists, the destination gets updated, otherwise it is added.
     */
    private void AddOrUpdateMovingProtein(int id, MovingProtein mP)
    {
        if (MovingProteins.ContainsKey(id)) MovingProteins[id].UpdateDestination(mP);
        else
        {
            mP.Reset();
            MovingProteins.Add(id, mP);
        }
    }

    /**
     * Method to switch between move and rotate handle for compartments
     */
    public void SwitchHandle()
    {
        if (compartmentMode || currentSelectionState == HandleSelectionState.Translate) ShowRotateHandle(currentlySelectedIngredientIndex);
        else ShowMoveHandle(currentlySelectedIngredientIndex);
    }

    /**
     * Shows the move handle for a given ingredient, identified by index (index in CPU Buffer)
     */
    public void ShowMoveHandle(int proteinIndex)
    {
        if (!compartmentMode && proteinIndex > 0 && !lockedIngredientIds.Contains((int)CPUBuffers.Get.ProteinInstancePositions[proteinIndex].w))
        {
            ingredientVariance.text = DecisionTreeManager.Get.GetIngredientVariance((int)CPUBuffers.Get.ProteinInstancePositions[proteinIndex].w).ToString();
            if (startPosition == Vector4.zero)
            {
                startPosition = CPUBuffers.Get.ProteinInstancePositions[proteinIndex];
                startRotation = CPUBuffers.Get.ProteinInstanceRotations[proteinIndex];
            }
            currentlySelectedIngredientIndex = proteinIndex;
            if (proteinIndex >= 0 && proteinIndex < CPUBuffers.Get.ProteinInstancePositions.Count())
            {
                transformHandle.Enable();
                transformHandle.SetSelectionState(HandleSelectionState.Translate);
                currentSelectionState = HandleSelectionState.Translate;
                transformHandle.transform.position = CPUBuffers.Get.ProteinInstancePositions[proteinIndex] * GlobalProperties.Get.Scale;
                transformHandle.transform.rotation = MyUtility.Vector4ToQuaternion(startRotation);
            }
            else
            {
                HideHandle();
            }
        }
        else if (compartmentMode) ShowRotateHandle(proteinIndex);
    }

    /**
     * Shows the rotate handle for a given ingredient, identified by index (index in CPU Buffer).
     */
    public void ShowRotateHandle(int proteinIndex)
    {
        if (compartmentMode || (!compartmentMode && proteinIndex > 0 && !lockedIngredientIds.Contains((int)CPUBuffers.Get.ProteinInstancePositions[proteinIndex].w)))
        {
            if (compartmentMode) membraneVariance.text = DecisionTreeManager.Get.GetIngredientVariance((int)CPUBuffers.Get.ProteinInstancePositions[proteinIndex].w).ToString();
            else ingredientVariance.text = DecisionTreeManager.Get.GetIngredientVariance((int)CPUBuffers.Get.ProteinInstancePositions[proteinIndex].w).ToString();

            if (startPosition == Vector4.zero)
            {
                startPosition = CPUBuffers.Get.ProteinInstancePositions[proteinIndex];
                startRotation = CPUBuffers.Get.ProteinInstanceRotations[proteinIndex];
            }
            currentlySelectedIngredientIndex = proteinIndex;
            if (proteinIndex >= 0 && proteinIndex < CPUBuffers.Get.ProteinInstancePositions.Count())
            {
                transformHandle.Enable();
                transformHandle.SetSelectionState(HandleSelectionState.Rotate);
                currentSelectionState = HandleSelectionState.Rotate;
                transformHandle.transform.position = CPUBuffers.Get.ProteinInstancePositions[proteinIndex] * GlobalProperties.Get.Scale;
                transformHandle.transform.rotation = MyUtility.Vector4ToQuaternion(startRotation);
            }
            else
            {
                HideHandle();
            }
        }
    }

    /**
     * Hides the transform handle, called when the user clicks on something different than the currently selected ingredient. This method
     * triggers an update of the changed ingredient positions, rotations, clusters, probabilities and so on.
     */
    public void HideHandle()
    {
        float variance = 0.0f;
        float.TryParse(ingredientVariance.text, out variance);

        if (!createTemplate)
        {
            if (currentlySelectedIngredientIndex != -1)
            {
                DecisionTreeManager.Get.SetIngridientVariance((int)CPUBuffers.Get.ProteinInstancePositions[currentlySelectedIngredientIndex].w, variance);
            }

            RecalculateProteinPositions();

            startPosition = Vector4.zero;
            //ingredientVariance.text = "";
        }
        currentlySelectedIngredientIndex = -1;
        currentSelectionState = HandleSelectionState.Translate;
        transformHandle.Disable();
    }

    /**
     * Invoked from method HideHandle. If the prosition and/or the rotation of the selected protein changed this method invokes the methods 
     * ModifyTreePositions and ModifyTreeRotations in DecisionTreeManager to adapt the probabilities of the decision tree. Afterwards for every protein of the same type 
     * as the modified this method invokes GetNewPositionAndRotation in DecisionTreeManager to get the new rotaion and positions for every protein. Afterwards the clusterinformation
     * is included and MovingProteins are updated.  
     */
    private void RecalculateProteinPositions()
    {
        if (currentlySelectedIngredientIndex < 0 || currentlySelectedIngredientIndex >= CPUBuffers.Get.ProteinInstancePositions.Count) return;
        //Reset cluster proteins if some are still in there from last update
        DecisionTreeManager.Get.clusterProteins.Clear();

        Vector4 endPosition = CPUBuffers.Get.ProteinInstancePositions[currentlySelectedIngredientIndex];
        Vector4 endRotation = CPUBuffers.Get.ProteinInstanceRotations[currentlySelectedIngredientIndex];

        float variance;
        float.TryParse(ingredientVariance.text, out variance);

        int ingredientId = (int)startPosition.w;

        //Moving proteins for redo and undo action
        Dictionary<int, MovingProtein> backupInfos = new Dictionary<int, MovingProtein>();
        Dictionary<int, MovingProtein> forwardInfos = new Dictionary<int, MovingProtein>();

        CustomPair<Action, Action> positionModification = null;
        CustomPair<Action, Action> rotationModification = null;

        //Almost equals because they are never exactly the same, also if not moved
        if (!CalculationUtility.AlmostEquals(startPosition, endPosition, 0.001f) || startRotation != endRotation)
        {
            bool isMoving = !CalculationUtility.AlmostEquals(startPosition, endPosition, 0.001f);
            bool isRotating = endRotation != startRotation;

            // hand over the positon of the protein to DecisionTreeManager which modifies the decision tree
            if (isMoving) positionModification = DecisionTreeManager.Get.ModifyTreePositions(new Vector4(endPosition.x * GlobalProperties.Get.Scale, endPosition.y * GlobalProperties.Get.Scale, endPosition.z * GlobalProperties.Get.Scale, endPosition.w));
            if (isRotating) rotationModification = DecisionTreeManager.Get.ModifyTreeRotations(new Vector3(endPosition.x * GlobalProperties.Get.Scale, endPosition.y * GlobalProperties.Get.Scale, endPosition.z * GlobalProperties.Get.Scale), endRotation, ingredientId);

            //TODO write comment why setting isRotation to true
            isRotating = true;
            List<TreeNode> decisionTree = DecisionTreeManager.Get.GetAllNodes(ingredientId);
            CheckFormClusterIntent(decisionTree, endPosition, ingredientId);

            String output1 = "";
            String output2 = "";

            for (int i = 0; i < CPUBuffers.Get.ProteinInstancePositions.Count; i++)
            {
                if (CPUBuffers.Get.ProteinInstancePositions[i].w == startPosition.w && i != currentlySelectedIngredientIndex)
                {
                    Vector4 currentStartPos = CPUBuffers.Get.ProteinInstancePositions[i];
                    Quaternion currentStartRot = MyUtility.Vector4ToQuaternion(CPUBuffers.Get.ProteinInstanceRotations[i]);
                    CustomPair<Vector4, Quaternion> positionRotationPair = DecisionTreeManager.Get.GetNewPositionAndRotation(decisionTree, currentStartPos, isMoving, isRotating, compartmentMode, startPosition);

                    Vector4 currentEndPos = positionRotationPair.GetFirst();
                    Quaternion currentEndRot = positionRotationPair.GetSecond();

                    if (isMoving && isRotating)
                    {
                        forwardInfos.Add(i, new MovingProtein(currentStartPos, currentEndPos, currentStartRot, currentEndRot));
                        backupInfos.Add(i, new MovingProtein(currentEndPos, currentStartPos, currentEndRot, currentStartRot));
                    }
                    else if (isMoving)
                    {
                        forwardInfos.Add(i, new MovingProtein(currentStartPos, currentEndPos));
                        backupInfos.Add(i, new MovingProtein(currentEndPos, currentStartPos));

                    }
                    else if (isRotating && !isMoving)
                    {
                        forwardInfos.Add(i, new MovingProtein(currentStartRot, currentEndRot));
                        backupInfos.Add(i, new MovingProtein(currentEndRot, currentStartRot));
                    }
                }
                else if (i == currentlySelectedIngredientIndex)
                {
                    CustomPair<Vector4, Quaternion> positionRotationPair = DecisionTreeManager.Get.GetNewPositionAndRotation(decisionTree, endPosition, false, true, compartmentMode, startPosition);

                    Vector4 currentEndPos = positionRotationPair.GetFirst();
                    Quaternion currentEndRot = (positionRotationPair.GetSecond() != Quaternion.identity ? positionRotationPair.GetSecond() : MyUtility.Vector4ToQuaternion(endRotation));
                    backupInfos.Add(i, new MovingProtein(endPosition, startPosition, currentEndRot, MyUtility.Vector4ToQuaternion(startRotation)));
                    forwardInfos.Add(i, new MovingProtein(startPosition, endPosition, MyUtility.Vector4ToQuaternion(startRotation), currentEndRot));
                }
            }
            //UnityEngine.Debug.Log("O1:"+output1);
            //UnityEngine.Debug.Log("O2:" + output2);
        }

        Dictionary<int, MovingProtein> additionals = DecisionTreeManager.Get.PerformUpdateForClustersAndCleanup(currentlySelectedIngredientIndex);
        foreach (KeyValuePair<int, MovingProtein> entry in additionals)
        {
            if (!forwardInfos.ContainsKey(entry.Key)) forwardInfos.Add(entry.Key, entry.Value);
            if (!backupInfos.ContainsKey(entry.Key)) backupInfos.Add(entry.Key, MovingProtein.CreateInverse(entry.Value));
        }

        Action forwardAction = delegate
        {
            foreach (KeyValuePair<int, MovingProtein> entry in forwardInfos) AddOrUpdateMovingProtein(entry.Key, entry.Value);
            if (positionModification != null) positionModification.GetForwardAction().Invoke();
            if (rotationModification != null) rotationModification.GetForwardAction().Invoke();
        };

        Action backAction = delegate
        {
            foreach (KeyValuePair<int, MovingProtein> entry in backupInfos) AddOrUpdateMovingProtein(entry.Key, entry.Value);
            if (positionModification != null) positionModification.GetBackwardAction().Invoke();
            if (rotationModification != null) rotationModification.GetBackwardAction().Invoke();
        };

        AddToBackstack(new CustomPair<Action, Action>(backAction, forwardAction), true);

        foreach (KeyValuePair<int, MovingProtein> entry in forwardInfos) AddOrUpdateMovingProtein(entry.Key, entry.Value);
    }

    /**
     * The method checks, if the user intended to model a cluster. A cluster is a set of 2 or more proteins. The last action is classified
     * as a -form cluster intent- if the user moves an ingredient inside the same compartment and takes the nearest neighbour as its 
     * cluster partner.
     */
    public void CheckFormClusterIntent(List<TreeNode> decisionTree, Vector4 endPosition, int ingredientId)
    {
        float prob = 0.0f;
        float random = UnityEngine.Random.Range(0.0f, 1.0f);
        CompartmentObject cO = null;
        //Used for blocking an update in the next step
        int ingredientIndex1 = currentlySelectedIngredientIndex;
        int ingredientIndex2 = -1;

        foreach (TreeNode n in decisionTree)
        {
            prob += n.GetProbability();
            if (random < prob || prob == 1.0f)
            {
                cO = n.GetCompartmentObject();
                if (cO != null && n.placedInside > 1 && Mathf.Sign(cO.GetSignedDistance(startPosition)) == Mathf.Sign(cO.GetSignedDistance(endPosition)))
                {
                    float distance = float.MaxValue;
                    int ingredientId2 = -1;
                    for (int i = 0; i < CPUBuffers.Get.ProteinInstancePositions.Count; i++)
                    {
                        if (i != currentlySelectedIngredientIndex && !lockedIngredientIds.Contains((int)CPUBuffers.Get.ProteinInstancePositions[i].w) && (endPosition - CPUBuffers.Get.ProteinInstancePositions[i]).magnitude < distance)
                        {
                            distance = (endPosition - CPUBuffers.Get.ProteinInstancePositions[i]).magnitude;
                            ingredientId2 = (int)CPUBuffers.Get.ProteinInstancePositions[i].w;
                            ingredientIndex2 = i;
                        }
                    }

                    UnityEngine.Debug.Log("Clustering id " + ingredientId + " and " + ingredientId2);

                    int clusterID = ClusterManager.FormCluster(n.GetClusterId(), ingredientId, ingredientId2, n.GetCompartmentObject(), distance);
                    n.SetClusterId(clusterID);

                    if (ingredientId2 != ingredientId)
                    {
                        List<TreeNode> decisionTree2 = DecisionTreeManager.Get.GetAllNodes(ingredientId2);
                        foreach (TreeNode n2 in decisionTree2)
                        {
                            if (cO.GetName().Equals(n2.GetCompartmentObject().GetName()))
                            {
                                n2.SetClusterId(clusterID);
                                break;
                            }
                        }
                    }
                }
                break;
            }
        }
    }

    /**
     * Method, used to add a compartment. This compartment is a root compartment and has no parent. If more root compartments are added,
     * overlapping is not prevented and the user has to place them accordingly. The type (shape) of the compartment is determined by the
     * dropdown value in the UI.
     */
    public void AddCompartment()
    {
        string id = membraneDropdown.options.ElementAt(membraneDropdown.value).text;
        if (!("Use template..".Equals(id))&& lockedIngredientIds.Contains(AvailableIngredients[id]._ingredientId)) return;

        List<Protein> template = null;

        if (useTemplate.Equals(id))
        {
            template = SceneFileUtility.ImportTemplate();
            if (template == null || template.Count == 0) return;
        }

        GameObject compartmentObject = new GameObject();
        CompartmentObject compartment;
        switch (compTypeDropdown.value)
        {
            case 0:
                compartment = (EllipsoidCompartment)compartmentObject.AddComponent<EllipsoidCompartment>();
                break;
            case 1:
                compartment = (CapsuleCompartment)compartmentObject.AddComponent<CapsuleCompartment>();
                break;
            default://case 2:
                compartment = (TorusCompartment)compartmentObject.AddComponent<TorusCompartment>();
                break;
        }

        compartment.SetName();
        CompartmentsListView.SelectedIndex = CompartmentsListView.Add(compartment.GetName());

        if (template != null)
        {
            compartment.SetMembrane(template);
            CompartmentObjectManager.Get.CompartmentObjects.Add(compartment);
            foreach (Protein p in template)
            {
                lockedIngredientIds.Add((int)p.Position.w);
                DecisionTreeManager.Get.CreateTree((int)p.Position.w);
            }
        }
        else
        {
            Ingredient toAdd = AvailableIngredients[id];
            lockedIngredientIds.Add(toAdd._ingredientId);
            compartment.SetMembrane(toAdd._ingredientId);
            CompartmentObjectManager.Get.CompartmentObjects.Add(compartment);
            DecisionTreeManager.Get.CreateTree(toAdd._ingredientId);
        }

        membraneMaxAmount.text = "" + (int)compartment.MEMBRANE_MAX;
    }

    /**
     * Method, used to add a subcompartment, by the means of a child of the currently selected compartment. A child fits inside their
     * respective parent. If no parent compartment exists this method calls the normal -addCompartment- method and adds it as a parent.
     * The type (shape) is determined by the dropdown value in the UI.
     */
    public void AddSubCompartment()
    {
        if (CompartmentsListView.DataSource.Count > 0)
        {
            string id = membraneDropdown.options.ElementAt(membraneDropdown.value).text;
            if (lockedIngredientIds.Contains(AvailableIngredients[id]._ingredientId)) return;

            List<Protein> template = null;

            if (useTemplate.Equals(id))
            {
                template = SceneFileUtility.ImportTemplate();
                if (template == null || template.Count == 0) return;
            }

            // selected list item 
            int index = CompartmentsListView.SelectedIndicies[0];
            CompartmentObject parentCompartment = CompartmentObjectManager.Get.GetSelectedCompartmentObject();

            //new subcompartment
            GameObject compartmentObject = new GameObject();
            CompartmentObject compartment;
            switch (compTypeDropdown.value)
            {
                case 0:
                    compartment = (EllipsoidCompartment)compartmentObject.AddComponent<EllipsoidCompartment>();
                    break;
                case 1:
                    compartment = (CapsuleCompartment)compartmentObject.AddComponent<CapsuleCompartment>();
                    break;
                default://case 2:
                    compartment = (TorusCompartment)compartmentObject.AddComponent<TorusCompartment>();
                    break;
            }

            parentCompartment.AddChild(compartment);
            compartment.AddParent(parentCompartment);

            if (template != null)
            {
                template = SceneFileUtility.ImportTemplate();
                compartment.SetMembrane(template);
                compartment.SetName();

                CompartmentsListView.SelectedIndex = CompartmentsListView.Add(compartment.GetName());
                CompartmentObjectManager.Get.CompartmentObjects.Add(compartment);

                List<CompartmentObject> obj = CompartmentObjectManager.Get.CompartmentObjects;
                foreach (Protein p in template)
                {
                    lockedIngredientIds.Add((int)p.Position.w);
                    DecisionTreeManager.Get.CreateTree((int)p.Position.w);
                }
            }
            else
            {
                Ingredient toAdd = AvailableIngredients[id];
                lockedIngredientIds.Add(toAdd._ingredientId);
                compartment.SetMembrane(toAdd._ingredientId);

                // add compartment name to list view 
                compartment.SetName();
                CompartmentsListView.SelectedIndex = CompartmentsListView.Add(compartment.GetName());

                // update the list of compartments in the compartment manager 
                CompartmentObjectManager.Get.CompartmentObjects.Add(compartment);
            }

            membraneMaxAmount.text = "" + (int)compartment.MEMBRANE_MAX;
        }
        else
        {
            AddCompartment();
        }
    }

    /**
     * Method, which is called by a change in the UI slider for membrane population. The slider amount gets mapped to the 
     * max amount of membrane ingredients and performs, depending on the change, a population or a depopulation of the membrane of
     * the currently selected compartment in the ListView (UI)
     */
    public void PopulateMembrane()
    {
        CompartmentObject selectedComp = null;

        var selected = CompartmentsListView.SelectedIndicies;
        foreach (var index in selected)
        {
            selectedComp = CompartmentObjectManager.Get.CompartmentObjects[index];
        }

        if (selectedComp != null)
        {
            int diff = (int)(selectedComp.MEMBRANE_MAX * membraneAmountSlider.value) - selectedComp.GetMembraneAmount();
            if (diff != 0)
            {
                if (diff > 0) selectedComp.PopulateBy(diff);
                else selectedComp.DepopulateBy(diff);
            }
        }
    }

    /**
     * The Slider in the UI, which is used to populate the membrane is bound to the currently selected compartment in the ListView (UI). This method 
     * is called in the update method to check the currently selected compartment and set the slider to the according value.
     */
    public void CheckFuzzy()
    {
        var selected = CompartmentsListView.SelectedIndicies;
        foreach (var index in selected)
        {
            try
            {
                if (index != lastSelectedCompartmentIndex)
                {
                    lastSelectedCompartmentIndex = index;
                    membraneMaxAmount.text = "" + (int)CompartmentObjectManager.Get.CompartmentObjects[index].MEMBRANE_MAX;
                }
                float maxValue = CompartmentObjectManager.Get.CompartmentObjects[index].MEMBRANE_MAX;
                float.TryParse(membraneMaxAmount.text, out maxValue);
                if (maxValue > 0.0f) CompartmentObjectManager.Get.CompartmentObjects[index].MEMBRANE_MAX = maxValue;
                membraneAmountSlider.value = CompartmentObjectManager.Get.CompartmentObjects[index].GetMembraneAmount() / CompartmentObjectManager.Get.CompartmentObjects[index].MEMBRANE_MAX;
            }
            catch
            {
            }
        }
    }

    /**
     * Downloads a given file from PDB database. The filename should consist of 4 letters, all files in pdb database consist of a 4-letter identification number
     */
    public void DownloadPDBFile()
    {
        string pdbName = (compartmentMode ? downloadPDBNameComp.text : downloadPDBNameIng.text);
        if (pdbName != null && pdbName.Length > 0)
        {
            currentMaxIngredientId++;

            Ingredient ingredient = new Ingredient();
            ingredient.name = pdbName;
            ingredient._ingredientId = currentMaxIngredientId;
            ingredient.path = Application.dataPath + "/../Data/proteins/" + ingredient.name + ".pdb";

            Source source = new Source();
            source._biomt = false;

            ingredient.source = source;
            ingredient.source.pdb = pdbName + ".pdb";

            InfoTextController.Get.InsertCustomIngredient(ingredient.name);

            Loaders.CellPackLoader.AddIngredient(ref ingredient);

            //At this point all ingredients are reloaded, sadly this is for now necessary, the assigned ID causes issues when reloading scenes, saving them etc.
            SceneManager.Get.IngredientGroups.Clear();
            SceneManager.Get.Ingredients.Clear();
            SceneManager.Get.AllIngredientNames.Clear();
            SceneManager.Get.ProteinIngredientNames.Clear();

            LoadAllPresentIngredients();
        }
    }

    /**
     * Delegates the call to CellPackLoader, loads all present ingredients the first time the user starts to model.
     */
    public void LoadAllPresentIngredients()
    {
        Loaders.CellPackLoader.LoadAllPresentIngredients();
    }

    /**
     * Sets the list of available ingredients to the Ingredient Groups in SceneManager. These ingredients can be used in the modelling process
     */
    public void SetAvailableIngredients(Dictionary<string, Ingredient> ingredients)
    {
        AvailableIngredients.Clear();
        AvailableIngredients = new Dictionary<string, Ingredient>(ingredients);

        currentMaxIngredientId = 0;
        foreach (Ingredient i in ingredients.Values)
        {
            IngredientGroup group = new IngredientGroup();
            group._ingredients = new List<Ingredient>();

            group._compartmentId = 0;
            group.unique_id = currentMaxIngredientId;
            group._groupType = -1;
            group.name = i.name;
            group.path = i.path;
            group.NumIngredients = 1;
            i._ingredientGroupId = currentMaxIngredientId;

            group._ingredients.Add(i);

            SceneManager.Get.IngredientGroups.Add(group);

            currentMaxIngredientId++;
        }

        RefreshConfiguration();
    }

    /**
     * Reloads the configuration file (ingredient-config.json), used to refresh ingredient names, descriptions and/or colors during runtime
     */
    public void RefreshConfiguration()
    {
        InfoTextController.Get.LoadIngredientInformation();

        Dictionary<string, Ingredient> updatedValues = new Dictionary<string, Ingredient>();
        foreach (KeyValuePair<string, Ingredient> pair in AvailableIngredients)
        {
            string infoName = InfoTextController.Get.GetIngredientNameForPdbName(pair.Value.source.pdb.Replace(".pdb", ""));
            if (updatedValues.ContainsKey(infoName)) infoName += "(" + pair.Value.source.pdb + ")";
            Ingredient value = pair.Value;
            value.name = infoName;
            updatedValues.Add(infoName, value);
        }
        AvailableIngredients.Clear();
        AvailableIngredients = updatedValues;

        membraneDropdown.ClearOptions();
        ingredientDropdown.ClearOptions();
        List<String> sorted = AvailableIngredients.Keys.ToList();
        sorted.Sort();

        //Add the -use template- option to the dropdowns
        membraneDropdown.options.Add(new Dropdown.OptionData() { text = useTemplate });

        //Only implemented for membrane, as normal ingredient in the scene some special handling has to be applied
        //ingredientDropdown.options.Add(new Dropdown.OptionData() { text = useTemplate });

        foreach (string s in sorted)
        {
            membraneDropdown.options.Add(new Dropdown.OptionData() { text = s });
            ingredientDropdown.options.Add(new Dropdown.OptionData() { text = s });
        }
    }

    /**
     * Returns the name of the ingredient, which is identified by the parameter @id
     */
    public string GetIngredientNameById(int id)
    {
        foreach (Ingredient i in AvailableIngredients.Values)
        {
            if (i._ingredientId == id) return i.source.pdb.Replace(".pdb", "");
        }
        return "";
    }

    /**
     * Returns a list of all available ingredient names
     */
    public List<string> GetAllIngredientNames()
    {
        return new List<string>(AvailableIngredients.Keys.ToArray());
    }

    /**
     * Returns the currently set variance in the UI for a certain ingredient
     */
    public float GetIngredientVariance()
    {
        return float.Parse(ingredientVariance.text);
    }

}