﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Assets.Scripts.Loaders;
using Assets.Scripts.Misc;

public class EllipsoidCompartment : CompartmentObject
{
    protected string NAME = "Ellipsoid";
    private static int ID = 0;

    public Vector3 radii = new Vector3(40.0f, 40.0f, 40.0f);

    public EllipsoidCompartment() { }

    // Use this for initialization
    void Start()
    {
        lastScale = transform.localScale;
        compartmentType = CompartmentType.Ellipsoid;
    }

    /**
     * Adapts all class members to the transform, defined by the type @state and a vector
     * containing the change
     */
    protected override void AdaptCompartmentToTransform(Vector4 transform, HandleSelectionState state)
    {        
        switch (state)
        {
            case HandleSelectionState.Scale:
                lastScale = handle.transform.localScale;
                radii = new Vector3(radii.x * transform.x, radii.y * transform.y, radii.z * transform.z);
                break;
            case HandleSelectionState.Translate:
                lastPosition = handle.transform.localPosition;
                center = new Vector3(center.x + transform.x, center.y + transform.y, center.z + transform.z);
                break;
            case HandleSelectionState.Rotate:
                lastRotation = transform;
                handle.transform.position = center;
                break;
        }
    }

    /**
      * Returns the signed distance for the point, considers the current rotation, other transforms are considered through the previous
      * modification of the member variables (i.e. radii).
      */
    public override float GetSignedDistance(Vector3 point)
    {
        Vector3 position = MyUtility.QuaternionTransform(Quaternion.Inverse(MyUtility.Vector4ToQuaternion(lastRotation)), point);
        return (float)(Vector3.Scale(position, new Vector3(1 / radii.x, 1 / radii.y, 1 / radii.z)).magnitude - 1.0) * Mathf.Min(Mathf.Min(radii.x, radii.y), radii.z);
    }

    /**
     * Halfs the size of the current compartment object, used for children, which should be smaller than their parent to prevent collision issues
     */
    public override void HalfSize()
    {
        Vector3 parentSize = parent.GetBoundsMax() - parent.GetBoundsMin();
        radii = parentSize / 4.0f;

        boundingBox.maxX = parent.GetBoundsMax() / 4.0f;
        boundingBox.maxY = parent.GetBoundsMax() / 4.0f;
        boundingBox.maxZ = parent.GetBoundsMax() / 4.0f;
        boundingBox.minX = parent.GetBoundsMin() / 4.0f;
        boundingBox.minY = parent.GetBoundsMin() / 4.0f;
        boundingBox.minZ = parent.GetBoundsMin() / 4.0f;
    }

    /**
     * Returns the name of the current compartment object. A bool can be passed to increment the static and unique compartment ID.
     */
    protected override string GetName(bool increment)
    {
        if (increment) ID++;
        return NAME + ID;
    }

    /**
     * Returns a possible child origin. For capsule the center is the best choice.
     */
    protected override Vector3 GetPossibleChildOrigin()
    {
        return center;
    }
}
