﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Newtonsoft.Json;
using Assets.Scripts.Utils;

namespace Assets.Scripts.Misc
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ProteinCluster
    {
        private static readonly int CLUSTER_MIN_SIZE = 2; //A cluster contains at least 2 elements

        [JsonProperty]
        private List<int> ingredientIds = new List<int>();
        [JsonProperty]
        public string compObjName;

        public CompartmentObject compartment;
        [JsonProperty]
        private PseudoCompartment pseudoCompartment = (PseudoCompartment)new GameObject().AddComponent<PseudoCompartment>();
        [JsonProperty]
        private float distanceToCenter;
        [JsonProperty]
        private int rotated = 0;
        [JsonProperty]
        private int clusterSize = CLUSTER_MIN_SIZE;

        [JsonProperty]
        public List<RotationProbabilityPair> clusterRotationPropabilities = new List<RotationProbabilityPair>();

        /**
         * Default constructor
         */
        public ProteinCluster()
        {
        }

        /**
         * Constructor to create a cluster with the ingredients @ingId1 and @ingId2. The reference compartment is used to keep 
         * the cluster within its bounds, the distance is the dinstance between the two cluster ingredients
         */
        public ProteinCluster(CompartmentObject refComp, float dist, int ingId1, int ingId2)
        {
            compartment = refComp;
            compObjName = refComp.name;
            distanceToCenter = dist;
            ingredientIds.Add(ingId1);
            ingredientIds.Add(ingId2);
        }

        /**
         * Returns whether a newly placed ingredient is within the threshold distance of the cluster, if so
         * the cluster size should be increased and the average distance should be actualized, otherwise the cluster
         * may be reduced or removed
         */
        public bool IsWithinThreshold(float distance)
        {
            return Math.Abs(distance) <= Math.Abs(2 * distanceToCenter);
        }

        /**
         * Increases the cluster size by 1 and adds the ingredient id to the cluster.
         */
        public void IncreaseClusterSize(int ingredientId)
        {
            clusterSize++;
            ingredientIds.Add(ingredientId);
        }

        /**
         * Adjusts the average distance from the center of the pseudo-compartment. All cluster ingredients will be within this distance.
         * Called to adjust the distance if @method isWithinThreshold returns true
         */
        public void AdjustDistance(float newDistance)
        {
            distanceToCenter += newDistance;
            pseudoCompartment.radius = distanceToCenter / clusterSize;
        }

        public void AddRotationProbabilityPair(RotationProbabilityPair rotation)
        {
            rotated++;
            rotation.relativeToCompartment = false;
            rotation.SetReferenceCompartment(pseudoCompartment);
            clusterRotationPropabilities.Add(rotation);
        }

        public List<RotationProbabilityPair> GetRotationProbabilities()
        {
            return clusterRotationPropabilities;
        }

        /**
         * Returns a pseudo compartment, which is no real compartment, it only holds the pseudo-center of the cluster ingredients
         */
        public CompartmentObject GetPseudoCompartment()
        {
            return pseudoCompartment;
        }

        /**
         * Decreases the cluster size if the user moves an ingredient too far away from the cluster. 
         * If the clustersize becomes smaller than 2 this method returns true to indicate that the 
         * cluster is no longer a cluster and should be removed
         */
        public bool DecreaseClusterSize(int ingredientId)
        {
            clusterSize--;
            ingredientIds.Remove(ingredientId);
            return clusterSize < CLUSTER_MIN_SIZE;
        }

        /**
         * Method returns a dictionary of ingredient ids and respective position and rotation, forming a cluster. The amount is determined by the cluster size
         */
        public List<CustomTriple<int, Vector4, Quaternion>> GetCluster()
        {
            List<CustomTriple<int, Vector4, Quaternion>> cluster = new List<CustomTriple<int, Vector4, Quaternion>>();

            float distance = distanceToCenter / clusterSize;

            Vector3 clusterPosition = CalculationUtility.DistributeRelativeToCompartment(compartment, Vector3.zero, 10f, true);
            Vector3 center = new Vector3(clusterPosition.x - distance, clusterPosition.y - distance, clusterPosition.z - distance);

            while (!(compartment.GetSignedDistance(center * GlobalProperties.Get.Scale) < 0.0f))
            {
                clusterPosition = CalculationUtility.DistributeRelativeToCompartment(compartment, Vector3.zero, 10f, true);
                center = new Vector3(clusterPosition.x - distance, clusterPosition.y - distance, clusterPosition.z - distance);
            }

            pseudoCompartment.SetCenter(center * GlobalProperties.Get.Scale);

            for (int i = 0; i < clusterSize; i++)
            {
                Vector4 position = Vector4.zero;
                bool isNewPos = false;
                int breakCounter = 0;
                while (!isNewPos || !(compartment.GetSignedDistance(position * GlobalProperties.Get.Scale) < 0.0f))
                {
                    position = GetRandomPosition(distance, center);
                    isNewPos = true;
                    foreach (CustomTriple<int, Vector4, Quaternion> pair in cluster)
                    {
                        if (pair.GetSecond() == position)
                        {
                            isNewPos = false;
                            break;
                        }
                    }
                    if (breakCounter > 1000) break;
                    breakCounter++;
                }
                position.w = ingredientIds[i];

                float probability = 0.0f;
                float random = UnityEngine.Random.Range(0.0f, 1.0f);
                RotationProbabilityPair rPP = null;

                foreach (RotationProbabilityPair pair in clusterRotationPropabilities)
                {
                    probability += (pair.rotated / rotated);
                    if (random < probability || probability == 1.0f)
                    {
                        rPP = pair;
                        break;
                    }
                }
                Quaternion rotation = (rPP != null ? MyUtility.Vector4ToQuaternion(CalculationUtility.GetNewRotation(position, rPP)) : Quaternion.identity);

                cluster.Add(new CustomTriple<int, Vector4, Quaternion>(ingredientIds[i], position, rotation));
            }
            return cluster;
        }

        /**
         * Returns a random position with the given distance from the center.
         */
        private Vector4 GetRandomPosition(float distance, Vector3 center)
        {
            int rand1 = UnityEngine.Random.Range(0.0f, 1.0f) < 0.5f ? -1 : 1;
            int rand2 = UnityEngine.Random.Range(0.0f, 1.0f) < 0.5f ? -1 : 1;
            int rand3 = UnityEngine.Random.Range(0.0f, 1.0f) < 0.5f ? -1 : 1;
            return new Vector4(rand1 * distance + center.x, rand2 * distance + center.y, rand3 * distance + center.z, 0);
        }

        /**
         * Returns the amount of ingredients within this cluster
         */
        public int GetClusterSize()
        {
            return clusterSize;
        }
    }
}
