﻿using System;
using UnityEngine;
using System.IO;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Schema;
using Assets.Scripts.Loaders;
using UIWidgets;
using UnityEngine.Assertions.Must;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using Assets.Scripts.Utils;

namespace Assets.Scripts.Misc
{
    /**
     * Object represents an ingredient in movement (rotation or translation). It has an optional start position and rotation, and an optional 
     * start and end rotation. For the animation duration these values are interpolated, the moving protein can be only moving, only rotating
     * or both. When it reaches its destination it gets destroyed.
     */
    public class MovingProtein
    {
        private static readonly float AnimationDuration = 1.0f;

        private Vector4 startPosition { get; set; }                 //Starting point for translation
        private Vector4 endPosition { get; set; }                   //End point (destination) for translation

        private Vector4 startRotation { get; set; }                 //Starting orientation for rotation
        private Vector4 endRotation { get; set; }                   //End orientation (destination) for rotation

        private Vector4 currentPosition;                            //Interpolated position (dependant on animation duration)
        private Vector4 currentRotation;                            //Interpolated rotation (dependant on animation duration)

        private bool moving;                                        //Whether ingredient is moving or not
        private bool rotating;                                      //Whether ingredient is rotating or not

        private float passedTime;                                   //Current timestamp for animation (between 0 and AnimationDuration)
        private bool movingFinished;                                //Whether destination is reached or not

        /**
         * Constructor for translating only
         */
        public MovingProtein(Vector4 startPos, Vector4 endPos)
        {
            startPosition = startPos;
            endPosition = endPos;
            passedTime = 0.0f;
            moving = true;
        }

        /**
         * Constructor for rotating only
         */
        public MovingProtein(Quaternion startRot, Quaternion endRot)
        {
            startRotation = MyUtility.QuanternionToVector4(startRot);
            endRotation = MyUtility.QuanternionToVector4(endRot);
            passedTime = 0.0f;
            rotating = true;
        }

        /**
         * Constructor for both translating and rotating
         */
        public MovingProtein(Vector4 startPos, Vector4 endPos, Quaternion startRot, Quaternion endRot)
        {
            startPosition = startPos;
            endPosition = endPos;
            startRotation = MyUtility.QuanternionToVector4(startRot);
            endRotation = MyUtility.QuanternionToVector4(endRot);
            passedTime = 0.0f;
            moving = true;
            rotating = true;
        }

        
        /**
         * Returns whether the protein has reached its destination
         */
        public bool HasReachedDestination()
        {
            InterpolateNextTimeStep();
           
            return movingFinished;
        }

        /**
         * Returns whether the protein is moving or not
         */
        public bool IsMoving()
        {
            return moving;
        }

        /**
         * Returns the interpolated position for translation for the current time stamp
         */
        public Vector4 GetInterpolatedPosition()
        {
            return currentPosition;
        }

        /**
         * Returns whether the protein is rotating or not
         */
        public bool IsRotating()
        {
            return rotating;
        }

        /**
         * Returns the interpolated rotation for the current time stamp
         */
        public Vector4 GetInterpolatedRotation()
        {
            return currentRotation;
        }

        /**
         * Called once per update, interpolates the next position and/or rotation, if finished it will be removed after updating
         */
        private void InterpolateNextTimeStep()
        {
            if (passedTime < AnimationDuration) passedTime += Time.deltaTime / AnimationDuration;
            else
            {
                passedTime = AnimationDuration;
                movingFinished = true;
            }

            if (moving) currentPosition = Vector4.Lerp(startPosition, endPosition, passedTime / AnimationDuration); // Set position proportional to passed time            
            if (rotating) currentRotation = Vector4.Lerp(startRotation, endRotation, passedTime / AnimationDuration);
        }

        /**
         * Updates the destination of this moving protein by another reference moving protein. This method is called, when a pending
         * movement is not yet completed, but the old destination is not valid anymore.
         */
        public void UpdateDestination(MovingProtein mP)
        {
            if (mP.IsMoving())
            {
                startPosition = currentPosition;
                endPosition = mP.endPosition;
            }
            if (mP.IsRotating())
            {
                startRotation = currentRotation;
                endRotation = mP.endRotation;
            }
            passedTime = 0.0f;
            movingFinished = false;
        }

        /**
         * Resets the protein, called for redo and undo actions to reset the time stamp as well as the position and rotation
         */
        public void Reset()
        {
            passedTime = 0.0f;
            currentPosition = startPosition;
            currentRotation = startRotation;
            movingFinished = false;
        }

        /**
         * Creates an inverse moving protein for the parameter moving protein mP. The returned moving protein moves from
         * endPosition to startPosition, same applies for the rotation.
         */
        public static MovingProtein CreateInverse(MovingProtein mP)
        {
            return new MovingProtein(mP.endPosition, mP.startPosition, MyUtility.Vector4ToQuaternion(mP.endRotation), MyUtility.Vector4ToQuaternion(mP.startRotation));
        }
    }
}