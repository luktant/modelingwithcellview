﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UIWidgets;

/**
 * The compartment object manager handles all compartment objects in the scene, it is responsible for showing and hiding the transform
 * handle, removing compartments, can be used to get the currently selected one from the list view in the UI.
 */
public class CompartmentObjectManager : MonoBehaviour
{
    private static CompartmentObjectManager _instance = null;
    public List<CompartmentObject> CompartmentObjects = new List<CompartmentObject>();
    public int SelectedCompartmentObject = 0;
    public ListView CompartmentsListView;
    public CompartmentObject transformableCompartment = null;

    public static CompartmentObjectManager Get
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<CompartmentObjectManager>();
                if (_instance == null)
                {
                    var go = GameObject.Find("CompartmentObjectManager");
                    if (go != null)
                    {
                        DestroyImmediate(go);
                    }
                    go = new GameObject("CompartmentObjectManager") { hideFlags = HideFlags.HideInInspector };
                    _instance = go.AddComponent<CompartmentObjectManager>();
                }
            }
            return _instance;
        }
    }

    /**
     * Returns the currently selected compartment object (selected in the list view in the UI)
     */
    public CompartmentObject GetSelectedCompartmentObject()
    {
        return CompartmentObjects[CompartmentsListView.SelectedIndex];
    }
     
 
    /**
     * Shows the move handle for the currently selected compartment object. Linked from the UI
     */
    public void ShowMoveHandle()
    {
        if (transformableCompartment != null)
        {
            transformableCompartment.ShowHandle(false, HandleSelectionState.Translate);
        }
        CompartmentObject compObj = GetSelectedCompartmentObject();
        //Dont allow moving of root object, makes the whole stuff simpler
        if (compObj.parent != null && (transformableCompartment != compObj || transformableCompartment.GetHandleSelectionState() != HandleSelectionState.Translate))
        {
            transformableCompartment = compObj;
            transformableCompartment.ShowHandle(true, HandleSelectionState.Translate);
        }
        else
        {
            transformableCompartment = null;
        }
    }

    /**
     * Shows the rotate handle for the currently selected compartment object. Linked from the UI
     */
    public void ShowRotateHandle()
    {
        if (transformableCompartment != null)
        {
            transformableCompartment.ShowHandle(false, HandleSelectionState.Translate);
        }
        if (transformableCompartment != GetSelectedCompartmentObject() || transformableCompartment.GetHandleSelectionState() != HandleSelectionState.Rotate)
        {
            transformableCompartment = GetSelectedCompartmentObject();
            transformableCompartment.ShowHandle(true, HandleSelectionState.Rotate);
        }
        else
        {
            transformableCompartment = null;
        }
    }

    /**
     * Shows the scale handle for the currently selected compartment object. Linked from the UI
     */
    public void ShowScaleHandle()
    {
        if (transformableCompartment != null)
        {
            transformableCompartment.ShowHandle(false, HandleSelectionState.Translate);
        }
        if (transformableCompartment != GetSelectedCompartmentObject() || transformableCompartment.GetHandleSelectionState() != HandleSelectionState.Scale)
        {
            transformableCompartment = GetSelectedCompartmentObject();
            transformableCompartment.ShowHandle(true, HandleSelectionState.Scale);
        }
        else
        {
            transformableCompartment = null;
        }
    }

    /**
     * Returns a list of all root compartment (compartments that have no parent).
     */
    public List<CompartmentObject> getRootCompartments()
    {
        List<CompartmentObject> rootCompartments = new List<CompartmentObject>();
        foreach (CompartmentObject cO in CompartmentObjects)
        {
            if (cO.GetParent() == null)
            {
                rootCompartments.Add(cO);
            }
        }
        return rootCompartments;
    }

    /**
     * Removes all compartments in the scene, should be called when a new scene is created
     */
    public void RemoveAllCompartments()
    {
        if (CompartmentObjects.Count > 0)
        {
            for (int index = CompartmentObjects.Count - 1; index >= 0; index--)
            {
                RemoveCompartment(index);
            }

            CompartmentObjects.Clear();
            CompartmentsListView.Clear();            
            CompartmentsListView.SelectedIndex = -1;
        }
    }

    /**
     * Removes the compartment, which is located in the UI listview at position @index
     */
    private void RemoveCompartment(int index)
    {
        transformableCompartment = null;
        if (index < CompartmentsListView.DataSource.Count)
        {
            CompartmentsListView.Remove(CompartmentsListView.DataSource[index]);
        }
        CompartmentObject compartmentToRemove = CompartmentObjects[index];
        CompartmentObjects.RemoveAt(index);
        CompartmentsListView.Remove(compartmentToRemove.name);
        compartmentToRemove.CleanBeforeDestroy();
        Destroy(compartmentToRemove.gameObject);
        compartmentToRemove = null;
    }

    /**
     * Removes the currently in the list view (UI) selected compartment
     */
    public void RemoveSelectedCompartment()
    {
        var cache = CompartmentsListView.SelectedIndex;

        if (CompartmentsListView.DataSource.Count >= 1)
        {
            var selected = CompartmentsListView.SelectedIndicies;
            foreach (var index in selected)
            {
                CompartmentObject compartmentToRemove = CompartmentObjects[index];

                UnityEngine.Debug.Log("Remove: " + compartmentToRemove.GetName());

                //Update Children
                if (compartmentToRemove.GetChildren().Count != 0)
                {
                    List<CompartmentObject> compartmentsToChange = compartmentToRemove.GetChildren();
                    for (int i = 0; i < compartmentsToChange.Count; i++)
                    {
                        if (compartmentsToChange[i] != null)
                        {
                            compartmentsToChange[i].AddParent(compartmentToRemove.GetParent());
                        }
                    }
                }

                RemoveCompartment(index);
            }
        }
        CompartmentsListView.SelectedIndex = Mathf.Min(cache, CompartmentsListView.DataSource.Count - 1);
    }

    /**
     * Iterates over all compartment objects and hides its respective handle
     */
    public void HideAllHandles()
    {
        foreach (CompartmentObject o in CompartmentObjects)
        {
            o.HideHandle();
        }
    }
}