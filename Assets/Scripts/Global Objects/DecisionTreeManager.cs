﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Assets.Scripts.Utils;
using Assets.Scripts.Misc;

/**
 * The decision tree manager holds all subtrees for all different ingredients in the scene. It is used to delegate modifications to the
 * respective tree nodes and returns for modifying actions a redo and undo operation for the Stack.
 */
public class DecisionTreeManager : MonoBehaviour
{

    // Declare manager as a singelton 
    private static DecisionTreeManager _instance = null;

    public Dictionary<int, TreeNode> rootNodes;
    //Stores the id of the protein and the amount of placed ones as the value
    public Dictionary<int, int> placedOveralls;

    //stores cluster protein positions and rotations
    public Dictionary<int, List<CustomPair<Vector4, Quaternion>>> clusterProteins = new Dictionary<int, List<CustomPair<Vector4, Quaternion>>>();

    // variances for kernel density estimation 
    public Dictionary<int, float> variances;

    public static DecisionTreeManager Get
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<DecisionTreeManager>();
                if (_instance == null)
                {
                    var go = GameObject.Find("DecisionTreeManager");
                    if (go != null)
                    {
                        DestroyImmediate(go);
                    }
                    go = new GameObject("DecisionTreeManager") { hideFlags = HideFlags.HideInInspector };
                    _instance = go.AddComponent<DecisionTreeManager>();
                }
            }
            return _instance;
        }
    }

    // Initialize member variabless
    void Start()
    {
        rootNodes = new Dictionary<int, TreeNode>();
        placedOveralls = new Dictionary<int, int>();
        variances = new Dictionary<int, float>();
    }

    // No update necessary
    void Update()
    {

    }

    /**
     * Method appends a subtree for a newly added ingredient, identified by its id. The corresponding variables are adjusted and the
     * subtree can be modified. Called on the first modification of the ingredients probabilities.
     */
    public void CreateTree(int ingredientId)
    {
        if (!rootNodes.ContainsKey(ingredientId))
        {
            placedOveralls.Add(ingredientId, 0);
            rootNodes.Add(ingredientId, new TreeNode(null, true, ingredientId));

            foreach (CompartmentObject cO in CompartmentObjectManager.Get.getRootCompartments())
            {
                rootNodes[ingredientId].AddChildNode(new TreeNode(cO, false, ingredientId));
            }
        }
    }

    /**
     * Resets the decision tree, deleting all the stored information. Should be called when creating a new scene or dimissing the previously
     * gathered probability informations.
     */
    public void Reset()
    {
        rootNodes.Clear();
        placedOveralls.Clear();
        clusterProteins.Clear();

        System.GC.Collect();
    }

    /**
     * Modifies the decision tree of the given ingredient. The position vector contains the 3D coordinates of the ingredient, the 4th value
     * is the ingredient ID. This information should be preserved at any point. Returns a pair of actions, a redo and an undo action for the
     * stack in the scene creator.
     */
    public CustomPair<Action, Action> ModifyTreePositions(Vector4 position)
    {
        int id = (int)position.w;
        if (!rootNodes.ContainsKey(id))
        {
            CreateTree(id);
        }

        Action forwardAction = new Action(delegate
        {
            rootNodes[id].ModifyTreeNodePositions(position);
            rootNodes[id].IncrementPlacedOverall();
        });
        Action backAction = new Action(delegate
        {
            rootNodes[id].UndoModifyTreeNodePositions(position);
            rootNodes[id].DecrementPlacedOverall();
        });

        forwardAction.Invoke();

        return new CustomPair<Action, Action>(backAction, forwardAction);
    }

    /**
     * Modifies the decision tree of the given ingredient. Changes the rotation probabilities of the given ingredient in the respective tree 
     * node (not for all instances). Returns a pair of actions, a redo and an undo action for the stack in the scene creator.
     */
    public CustomPair<Action, Action> ModifyTreeRotations(Vector4 position, Vector4 rotation, int id)
    {
        if (!rootNodes.ContainsKey(id))
        {
            CreateTree(id);
        }

        Quaternion rotQuat = new Quaternion(rotation.x, rotation.y, rotation.z, rotation.w);
        Matrix4x4 qToM = MyUtility.quatToMatrix(rotQuat);

        Action forwardAction = new Action(delegate
        {
            rootNodes[id].ModifyTreeNodeRotationProbabilities(position, rotQuat);
        });
        Action backAction = new Action(delegate
        {
            rootNodes[id].UndoModifyTreeNodeRotationProbabilities(position, rotQuat);
        });

        forwardAction.Invoke();

        return new CustomPair<Action, Action>(backAction, forwardAction);
    }

    /**
     * Returns a custom pair, holding a new position and rotation for the given ingredient. The parameter decision tree is the ingredients subtree, reducing the amount of nodes that
     * have to be tested. The parameter oldPosition is returned unmodified, if no movement happened. The boolean isMoving and isRotating are used to get a new position or a new 
     * orientation if needed. 
     */
    public CustomPair<Vector4, Quaternion> GetNewPositionAndRotation(List<TreeNode> decisionTree, Vector4 oldPosition, bool isMoving, bool isRotating, bool membraneProtein, Vector4 referenceStartPosition)
    {
        int ingredientId = (int)referenceStartPosition.w;
        bool onMembran = false;
        bool inside = true;
        bool isCluster = false;

        Vector4 currentEndPos = oldPosition;
        Quaternion currentEndRot = Quaternion.identity;

        TreeNode node = null;
        Quaternion test = Quaternion.identity;

        float random = UnityEngine.Random.Range(0.0f, 1.0f);
        float testedProbs = 0.0f;

        CompartmentObject cO = null;
        RotationProbabilityPair rPP = null;

        foreach (TreeNode n in decisionTree)
        {
            testedProbs += n.GetProbability();
            if (random < testedProbs || testedProbs == 1.0f)
            {
                if (n.IsClustered())
                {
                    isCluster = true;
                    if (!clusterProteins.ContainsKey(ingredientId))
                    {
                        List<CustomTriple<int, Vector4, Quaternion>> cluster = n.GetClusterPosAndRot();
                        foreach (CustomTriple<int, Vector4, Quaternion> triple in cluster)
                        {
                            if (clusterProteins.ContainsKey(triple.GetFirst()))
                            {
                                clusterProteins[triple.GetFirst()].Add(triple.CreateCustomPairFromValues());
                            }
                            else
                            {
                                List<CustomPair<Vector4, Quaternion>> newEntry = new List<CustomPair<Vector4, Quaternion>>();
                                newEntry.Add(triple.CreateCustomPairFromValues());
                                clusterProteins.Add(triple.GetFirst(), newEntry);
                            }
                        }
                    }
                    break;
                }

                cO = n.GetCompartmentObject();
                if (isRotating)
                {
                    rPP = GetRotationProbabilityPair(n);
                    node = n;
                }
                break;
            }
            testedProbs += n.GetProbabilityForMembran(membraneProtein);
            if (random < testedProbs || testedProbs == 1.0f)
            {
                if (n.IsClustered())
                {
                    isCluster = true;
                    if (!clusterProteins.ContainsKey(ingredientId))
                    {
                        List<CustomTriple<int, Vector4, Quaternion>> cluster = n.GetClusterPosAndRot();
                        foreach (CustomTriple<int, Vector4, Quaternion> triple in cluster)
                        {
                            if (clusterProteins.ContainsKey(triple.GetFirst()))
                            {
                                clusterProteins[triple.GetFirst()].Add(triple.CreateCustomPairFromValues());
                            }
                            else
                            {
                                List<CustomPair<Vector4, Quaternion>> newEntry = new List<CustomPair<Vector4, Quaternion>>();
                                newEntry.Add(triple.CreateCustomPairFromValues());
                                clusterProteins.Add(triple.GetFirst(), newEntry);
                            }
                        }
                    }
                    break;
                }
                cO = n.GetCompartmentObject();
                onMembran = true;
                rPP = GetRotationProbabilityPair(n);
                node = n;
                break;
            }
        }

        if (isCluster && clusterProteins.ContainsKey(ingredientId))
        {
            currentEndPos = clusterProteins[ingredientId][0].GetFirst();
            currentEndRot = clusterProteins[ingredientId][0].GetSecond();
            clusterProteins[ingredientId].RemoveAt(0);
            if (clusterProteins[ingredientId].Count == 0) clusterProteins.Remove(ingredientId);
        }
        else
        {
            if (isMoving)
            {
                if (cO == null)
                {
                    cO = rootNodes[ingredientId].childNodes[0].GetCompartmentObject();
                    inside = false;
                }
                if (onMembran) currentEndPos = CalculationUtility.DistributeOnSurface(cO, referenceStartPosition);
                else currentEndPos = CalculationUtility.DistributeRelativeToCompartment(cO, referenceStartPosition, 10.0f, inside);
            }
        }

        if (isRotating && rPP != null && node != null)
        {
            //currentEndRot = MyUtility.Vector4ToQuaternion(CalculationUtility.GetNewRotation(currentEndPos, rPP));
            currentEndRot = GetNewRotation(currentEndPos, node, rPP);
        }
        return new CustomPair<Vector4, Quaternion>(currentEndPos, currentEndRot);
    }

    /**
     * Normal update methods do update only position/rotation of one ingredient type (the changed one). But due to possible cluster 
     * formations the in the cluster contained ingredients have to be updated to, otherwise no cluster will be created.
     * Call this method after the GetNewPositionAndRotation method, the "clusterProteins" contain all the relevant information about 
     * ingredients, that should be moved.
     */
    public Dictionary<int, MovingProtein> PerformUpdateForClustersAndCleanup(int indexOfPlacedProtein)
    {
        Dictionary<int, MovingProtein> stillToMove = new Dictionary<int, MovingProtein>();
        if (clusterProteins.Count == 0) return stillToMove;

        for (int i = 0; i < CPUBuffers.Get.ProteinInstancePositions.Count; i++)
        {
            int id = (int)CPUBuffers.Get.ProteinInstancePositions[i].w;
            if (clusterProteins.ContainsKey(id))
            {
                stillToMove.Add(i, new MovingProtein(CPUBuffers.Get.ProteinInstancePositions[i], clusterProteins[id][0].GetFirst(), MyUtility.Vector4ToQuaternion(CPUBuffers.Get.ProteinInstanceRotations[i]), clusterProteins[id][0].GetSecond()));

                clusterProteins[id].RemoveAt(0);
                if (clusterProteins[id].Count == 0) clusterProteins.Remove(id);
            }
        }
        clusterProteins.Clear();
        return stillToMove;
    }

    private Quaternion GetNewRotation(Vector4 position, TreeNode n, RotationProbabilityPair rPP)
    {
        Vector3 nearestSurfacePoint = Vector3.Normalize(CalculationUtility.GetNearestSurfacePoint(rPP.referenceCompartment, position, Vector3.zero));
        Vector3 normalVector = Vector3.Normalize(CalculationUtility.GetNormalVectorNew(rPP.referenceCompartment, nearestSurfacePoint));
        Dictionary<Quaternion, int> samples = n.GetSamples();
        Quaternion q = Quaternion.identity;
        if (GetIngredientVariance((int)position.w) != 0)
        {
            q = n.GetRandomKDESample();
        }
        else
        {
            float randomValue = UnityEngine.Random.Range(0.0f, 1.0f);
            float probability = 0.0f;

            foreach (KeyValuePair<Quaternion, int> kvpair in samples)
            {
                probability += (kvpair.Value / n.sampleAmount);
                if (randomValue < probability || probability == 1.0f)
                {
                    q = kvpair.Key;
                    break;
                }
            }
        }

        Quaternion fromReferenceToTarget = Quaternion.FromToRotation(new Vector3(0, 1, 0), normalVector);
        Quaternion endRotation = fromReferenceToTarget * q;
        return endRotation;
    }

    private RotationProbabilityPair GetRotationProbabilityPair(TreeNode n)
    {
        float randomValue = UnityEngine.Random.Range(0.0f, 1.0f);
        float probability = 0.0f;

        if (!n.IsClustered())
        {
            foreach (RotationProbabilityPair pair in n.GetRotationProbability())
            {
                probability += (pair.rotated / n.rotated);
                if (randomValue < probability || probability == 1.0f)
                {
                    return pair;
                }
            }
        }
        else
        {
            foreach (RotationProbabilityPair pair in ClusterManager.GetClusterForId(n.GetClusterId()).GetRotationProbabilities())
            {
                probability += (pair.rotated / n.rotated);
                if (randomValue < probability || probability == 1.0f)
                {
                    return pair;
                }
            }
        }
        return null;
    }


    /**
     * Returns a list of all nodes of the subtree of the given ingredient. The ingredientID is used to identify the subtree
     */
    public List<TreeNode> GetAllNodes(int ingredientId)
    {
        if (!rootNodes.ContainsKey(ingredientId)) CreateTree(ingredientId);

        List<TreeNode> nodeList = rootNodes[ingredientId].GetNodes();
        // just debug output to  check if the propabilities are corect
        String output = "Probabilities: ";
        String output2 = "Rotation: ";
        foreach (TreeNode t in nodeList)
        {
            output += t.GetCompartmentObject().GetName() + ": " + t.GetProbability() + "; " + "MembranP: " + t.GetProbabilityForMembran(false);
            List<RotationProbabilityPair> list = t.GetRotationProbability();
            output2 += t.GetCompartmentObject().GetName() + " ";
            foreach (RotationProbabilityPair p in list)
            {
                output2 += "rotationProtein: (" + p.rotation.x + ", " + p.rotation.y + ", " + p.rotation.z + ", " + p.rotation.w + ")\n" +
                    "rotatedInside: " + p.rotated + " rotatedOverall: " + t.rotated + " Probability: " + (p.rotated / t.rotated) + "\n";
            }
        }
        UnityEngine.Debug.Log(output + " outside: " + rootNodes[ingredientId].GetProbability());
        UnityEngine.Debug.Log(output2);
        nodeList.Add(rootNodes[ingredientId]);
        return nodeList;
    }

    /**
     * Returns the root node of the ingredients subtree, identified by the ingredientId
     */
    public TreeNode GetRootNode(int ingredientId)
    {
        return rootNodes[ingredientId];
    }

    /**
     * Returns the ingredients variance, which can be set in the process of adding new ingredients.
     */
    public float GetIngredientVariance(int ingredientId)
    {
        if (!variances.ContainsKey(ingredientId))
        {
            return 0.0f;
        }
        else
        {
            return variances[ingredientId];
        }
    }

    /**
     * Sets the ingredients variance. Each ingredient can have only one variance for now, could be extended to store the variance depending on the tree node.
     */
    public void SetIngridientVariance(int ingrediantId, float variance)
    {
        if (!variances.ContainsKey(ingrediantId))
        {
            variances.Add(ingrediantId, variance);
        }
        else
        {
            variances[ingrediantId] = variance;
        }
    }

}
